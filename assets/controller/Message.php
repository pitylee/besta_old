<?php
class BST_Message{
	public function __construct($config,$session,$SE, $UNID, $URL, $DT){
		$this->config = $config;
		$this->session = $session;
		$this->UNID = $UNID;
		$this->DT = $DT;
		$this->template = "<b>%s</b><br/>";
		$this->prefix =  "<div class=\"error\">" . $DT->ini("NAME_MESSAGE", "MESSAGE");
		$this->suffix = "</div>";
	}
	
	public function set($type="info", $msgtype="text", $message=""){
		if( $message == "" ){ $message = $msgtype; $msgtype = "text"; }
		
		$this->SE = $this->session->read($this->UNID);
		if( !isset( $this->SE["Message"][$type] ) || is_array( $this->SE["Message"][$type] ) === false ) $this->SE["Message"][$type] = array();
		$message = array("printin"=>$msgtype, "message"=>$message);
		array_push($this->SE["Message"][$type],$message);
		
		$this->session->write($this->UNID,$this->SE);
		
	}
	
	public function get($type){
		$this->SE = $this->session->read($this->UNID);
		if(!empty($this->SE["Message"])){
			if( $type ==  "ajax" ){
				$messagee = "";
				
				foreach($this->SE["Message"] as $msgtype=>$array){
					foreach($this->SE["Message"][$msgtype] as $key=>$message){
						$printin = $message["printin"];
						$message = $message["message"];
						
						if( $printin == $type ){
							$messagee .= $message . "</br>";
							unset($this->SE["Message"][$msgtype][$key]);
							$this->session->write($this->UNID,$this->SE);
						}
					}
					
					if($messagee != ""){
						echo "<div class=\"BST_Message BST_Message-".$msgtype."\" style=\"display:none;\">". $messagee ."</div>";
					}
					$messagee = "";
				}
				
			}
			elseif($type=="script"){
				foreach($this->SE["Message"] as $msgtype=>$array){
					foreach($this->SE["Message"][$msgtype] as $key=>$message){
						$printin = $message["printin"];
						$message = $message["message"];
						
						if( $printin == $type ){
							if(hasTags($message) === true){
								if( strpos($message, "<".$type) !== false){
									print($message);
									
									unset($this->SE["Message"][$msgtype][$key]);
									$this->session->write($this->UNID,$this->SE);
								}
							}
						}
					}
				}
			}
			elseif( $type == "plain" ){
				foreach($this->SE["Message"] as $msgtype=>$array){
					foreach($this->SE["Message"][$msgtype] as $key=>$message){
						$printin = $message["printin"];
						$message = $message["message"];
						if( $printin == $type ){
							if(hasTags($message) === false){
								echo $message;
									
									unset($this->SE["Message"][$msgtype][$key]);
									$this->session->write($this->UNID,$this->SE);
							}
						}
					}
				}
			}
			else{
				foreach($this->SE["Message"] as $msgtype=>$array){
					foreach($this->SE["Message"][$msgtype] as $key=>$message){
						$printin = $message["printin"];
						$message = $message["message"];
						
							if(hasTags($message) === false){
								printf($this->prefix);
								printf($this->template,$message);
								printf($this->suffix);
									
									unset($this->SE["Message"][$msgtype][$key]);
									$this->session->write($this->UNID,$this->SE);
							}
					}
				}
			}		
			
		}
		else{
			return false;
		}
	}
}
?>