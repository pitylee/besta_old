<?php
class BST_Error {
	var $errno;
	var $errstr;
	var $errfile;
	var $errline;
	var $errcontext;
	
	function __construct($ip=0, $show_user=1, $show_developer=1, $email=NULL, $log_file=NULL, $DB, $DT){
		$this->ip = $ip;
		$this->show_user = $show_user;
		$this->show_developer = $show_developer;
		$this->email = mysql_real_escape_string($email);
		$this->log_file = realpath($log_file);
		$this->log_message = NULL;
		$this->email_sent = false;
		$this->DB = $DB;
		$this->DT = $DT;

		$this->error_codes =  E_ERROR | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR;
		$this->warning_codes =  E_WARNING | E_CORE_WARNING | E_COMPILE_WARNING | E_USER_WARNING;

		//associate error codes with errno...
		$this->error_names = array('E_ERROR','E_WARNING','E_PARSE','E_NOTICE','E_CORE_ERROR','E_CORE_WARNING',
								   'E_COMPILE_ERROR','E_COMPILE_WARNING','E_USER_ERROR','E_USER_WARNING',
								   'E_USER_NOTICE','E_STRICT','E_RECOVERABLE_ERROR');

		for($i=0,$j=1,$num=count($this->error_names); $i<$num; $i++,$j=$j*2)
			$this->error_numbers[$j] = $this->error_names[$i];
			
	}
	
	function set(){
		set_error_handler(array(&$this, "handler"));
	}
	
	function handler($errno, $errstr, $errfile, $errline, $errcontext){
		$this->errno = $errno;
		$this->errstr = $errstr;
		$this->errfile = $errfile;
		$this->errline = $errline;
		$this->errcontext = $errcontext;
		
		if($this->log_file)
			$this->log_error_msg();

		if($this->email)
			$this->send_error_msg();
		/*
		if($this->show_user)
			$this->error_msg_basic();
		else
			$this->error_msg_detailed();
		*/
	   /* Don't execute PHP internal error handler */
	   return true;
	}

	function show($type, $separator=NULL, $method=NULL){
		$message = NULL;
		$silent = (2 & $this->show_developer)?true:false;
		$context = (4 & $this->show_developer)?true:false;
		$backtrace = (8 & $this->show_developer)?true:false;
		
		if( $this->errno ){
			if( $type == "plane" ){
				$message = $this->errfile . ":" . $this->errline . $separator ;
				$message .= $this->DT->ini("NAME_CODE", "ERROR") . $this->error_numbers[$this->errno] . $separator ;
				$message .= $this->DT->ini("NAME_MESSAGE", "ERROR") . $this->errstr . $separator ;
				
				if( $method == "return" ){
					return $message;
				}
				else{
					echo $message;
				}
				
			}
			elseif( $type == "detailed" ){

				switch(true) {
					case (16 & $this->show_developer): $color='white'; break;
					case (32 & $this->show_developer): $color='black'; break;
					default: $color='red';
				}
				
				$message =  ($silent)?"<!--\n":'';
				$message .= "<div class=\"site_error\"><pre>\n\n";
				$message .= $this->DT->ini("NAME_FILE", "ERROR").print_r( $this->errfile, true)."\n";
				$message .= $this->DT->ini("NAME_LINE", "ERROR").print_r( $this->errline, true)."\n\n";
				$message .= $this->DT->ini("NAME_CODE", "ERROR").print_r( $this->error_numbers[$this->errno], true)."\n";
				$message .= $this->DT->ini("NAME_MESSAGE", "ERROR").print_r( $this->errstr, true)."\n\n";
				$message .= ($context)?$this->DT->ini("NAME_CONTEXT", "ERROR").print_r( $this->errcontext, true)."\n\n":'';
				$message .= ($backtrace)?$this->DT->ini("NAME_BACKTRACE", "ERROR").print_r( debug_backtrace(), true)."\n\n":'';
				$message .= "</pre>\n</div>";
				$message .= ($silent)?"-->\n\n":'';

				if( $method == "return" ){
					return $message;
				}
				else{
					echo $message;
				}
			}
			else{ // if basic or null
				if($this->errno & $this->error_codes) $message .= "<b>".$this->DT->ini("NAME_ERROR", "ERROR")."</b> ".$this->DT->ini("ERROR_MESSAGE");
				if($this->errno & $this->warning_codes) $message .= "<b>".$this->DT->ini("NAME_WARNING", "ERROR")."</b> ".$this->DT->ini("ERROR_MESSAGE");

				if($message) $message .= ($this->email_sent)?$this->DT->ini("ERROR_DEVELOPER_NOTIFIED", "ERROR")."<br />\n":"<br />\n";
				
				if( $method == "return" ){
					return $message;
				}
				else{
					echo $message;
				}
			}
		}
	}	
	
	function error_msg_plane($separator){
		$message = NULL;
		$context = (4 & $this->show_developer)?true:false;
		$backtrace = (8 & $this->show_developer)?true:false;
		
		if($this->errno){
			$message = $this->errfile . $separator ;
			$message .= $this->DT->ini("NAME_LINE", "ERROR") . $this->errline . $separator ;
			$message .= $this->DT->ini("NAME_CODE", "ERROR") . $this->error_numbers[$this->errno] . $separator ;
			$message .= $this->DT->ini("NAME_MESSAGE", "ERROR") . $this->errstr . $separator ;
		}
		echo $message;
	}
	
	function error_msg_basic() {
		$message = NULL;
		if($this->errno & $this->error_codes) $message .= "<b>".$this->DT->ini("NAME_ERROR", "ERROR")."</b> ".$this->DT->ini("ERROR_MESSAGE", "ERROR");
		if($this->errno & $this->warning_codes) $message .= "<b>".$this->DT->ini("NAME_WARNING", "ERROR")."</b> ".$this->DT->ini("ERROR_MESSAGE", "ERROR");

		if($message) $message .= ($this->email_sent)?$this->DT->ini("ERROR_DEVELOPER_NOTIFIED", "ERROR")."<br />\n":"<br />\n";
		echo $message;
	}

	function error_msg_detailed() {
		//settings for error display...
		$silent = (2 & $this->show_developer)?true:false;
		$context = (4 & $this->show_developer)?true:false;
		$backtrace = (8 & $this->show_developer)?true:false;

		switch(true) {
			case (16 & $this->show_developer): $color='white'; break;
			case (32 & $this->show_developer): $color='black'; break;
			default: $color='red';
		}

		if($this->errno){
			$message =  ($silent)?"<!--\n":'';
			$message .= "<div class=\"site_error\"><pre>\n\n";
			$message .= $this->DT->ini("NAME_FILE", "ERROR").print_r( $this->errfile, true)."\n";
			$message .= $this->DT->ini("NAME_LINE", "ERROR").print_r( $this->errline, true)."\n\n";
			$message .= $this->DT->ini("NAME_CODE", "ERROR").print_r( $this->error_numbers[$this->errno], true)."\n";
			$message .= $this->DT->ini("NAME_MESSAGE", "ERROR").print_r( $this->errstr, true)."\n\n";
			$message .= ($context)?$this->DT->ini("NAME_CONTEXT", "ERROR").print_r( $this->errcontext, true)."\n\n":'';
			$message .= ($backtrace)?$this->DT->ini("NAME_BACKTRACE", "ERROR").print_r( debug_backtrace(), true)."\n\n":'';
			$message .= "</pre>\n";
			$message .= ($silent)?"-->\n\n":'';

		echo $message;
		}
	}

	function send_error_msg(){
		if($this->errno){
			$message = $this->DT->ini("NAME_FILE", "ERROR").print_r( $this->errfile, true)."\n";
			$message .= $this->DT->ini("NAME_LINE", "ERROR").print_r( $this->errline, true)."\n\n";
			$message .= $this->DT->ini("NAME_CODE", "ERROR").print_r( $this->error_numbers[$this->errno], true)."\n";
			$message .= $this->DT->ini("NAME_MESSAGE", "ERROR").print_r( $this->errstr, true)."\n\n";
			$message .= "log: ".print_r( $this->log_message, true)."\n\n";
			$message .= $this->DT->ini("NAME_CONTEXT").print_r( $this->errcontext, true)."\n\n";
			//$message .= $this->DT->ini("NAME_BACKTRACE").print_r( $this->debug_backtrace(), true)."\n\n";

			$this->email_sent = false;
			if(mail($this->email, 'Error: '.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], $message, "From: error@".$_SERVER['HTTP_HOST']."\r\n"))
				$this->email_sent = true;
		}
	}

	function log_error_msg() {
		if($this->errno){
			$message =  "time: ".date("j M y - g:i:s A (T)", mktime())."\n";
			$message .= $this->DT->ini("NAME_FILE", "ERROR").print_r( $this->errfile, true)."\n";
			$message .= $this->DT->ini("NAME_LINE", "ERROR").print_r( $this->errline, true)."\n\n";
			$message .= $this->DT->ini("NAME_CODE", "ERROR").print_r( $this->error_numbers[$this->errno], true)."\n";
			$message .= $this->DT->ini("NAME_MESSAGE", "ERROR").print_r( $this->errstr, true)."\n";
			$message .= "##################################################\n\n";

			if (!$fp = fopen($this->log_file, 'a+'))
				$this->log_message = $this->DT->ini("LOG_ERROR_OPEN", "ERROR"). $this->log_file; $log_error = true;

			if (!fwrite($fp, $message))
				$this->log_message = $this->DT->ini("LOG_WRITE_ERROR", "ERROR") . $this->log_file; $log_error = true;

			if(!$this->log_message)
				$this->log_message = $this->DT->ini("LOG_SUCCESS", "ERROR") . $this->log_file;

			fclose($fp);
		}
	}	

}
?>