<?php
class BST_Builder{
	function __construct($DB){
		$this->DB = $DB;
	}
	
	public function initialize($file){
		$this->config = parse_ini_file($_SERVER["DOCUMENT_ROOT"].$file, true);
		return $this->config;
	}

	public function template($less){
		if( isset($_GET["admin"]) ){
			$templateini = $this->path($this->config("URL", "ADMIN"),$this->config("TEMPLATE_DIR", "PATHS"),$this->config("template", "current"))."template.details.ini";
		}
		else{
			$templateini = $this->path($this->config("TEMPLATE_DIR", "PATHS"),$this->config("template", "current"))."template.details.ini";
		}
		
		if( is_file($templateini) ){
			$template = parse_ini_file($templateini, true);
			
			foreach( $template as $key=>$value ){
				// Check if hexa color, and add a # if it has not
				if( has($key, "color") && is_hex($value) ){
					if( $value[0] != "#" ){
						$template[$key]= "#" . $value;
					}
				}
			}
			
			// Push the variables to the less!
			$less->setVariables($template);
			
		}
		else{
			return null;
		}
		
		return $template;
	}
	
	public function path(){
		$arguments = func_get_args();

		// Give the docroot to the path initially
		if( strtoupper($arguments[0]) == "NOROOT" ){ 
			unset($arguments[0]);
			$path = "/";
		}
		else{
			$last = $arguments[0][strlen($arguments[0])-1];
			if( $last == "/" ){ // if arg ends in / do not add slash
				$path = $this->config["DOCUMENT_ROOT"];
			}
			else{
				$path = $this->config["DOCUMENT_ROOT"] . "/";
			}
		}
		
		// Add arguments to path
		foreach( $arguments as $value ){
			$last = $value[strlen($value)-1];
			
			if( $last == "/" ){ // if arg ends in / do not add slash
				$path .= $value;
			}
			else{
				$path .= $value . "/";
			}
		}
		
		// Remove accidental double slashes
		$path = str_replace("//", "/", $path);

		// return the generated path
		return $path;
	}
	
	public function config($which, $section=null){
		$section = ( strtoupper($section) == "CURRENT" ) ? strtoupper($GLOBALS["currsite"]) : strtoupper(str_replace(" ", "", $section));
		$which = strtoupper(str_replace(" ", "", $which));
		
		if( isset($which) && !empty($which) ){
			if( $section ){
				if( isset($this->config[$section][$which]) ){
					return $this->config[$section][$which];
				}
				else{
					exit("Configuration \"<b>".$which."</b>\" for section \"<b>[".$section."]</b>\" not found!");
				}
			}
			else{
				if( isset($this->config[$which]) ){
					return $this->config[$which];
				}
				else{
					exit("Global configuration \"<b>".$which."</b>\" not found!");
				}
				return $this->config[$which];
			}
		}
		else{
			exit("Parameter for config is missing!");
		}
	}


	
}
?>