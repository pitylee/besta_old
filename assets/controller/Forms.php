<?php
class BST_Forms{
	function __construct($config,$REQUEST, $DT){
		$this->REQUEST = $REQUEST;
		$this->DT = $DT;
		if(isset($this->REQUEST["submit"])){ $this->submit = $this->REQUEST["submit"]; }
	}
	
	public function submitForm(){
		if( isset($this->submit) ){
			return true; 
		}
		else{ return false; }
	}
	
	private function validate($name, $return){
		if(!isset($this->REQUEST[$name])){
			echo $this->DT->ini("FORM_FIELD_NOT_EXIST");
		}
		else{
			if(empty($this->REQUEST[$name])){
				echo $this->DT->ini("FORM_FIELD_EMPTY") . $name;
			}
			else{
				return $return;
			}
		}	
	}
	
	public function text($name){
		$return = mysql_escape($_REQUEST[$name]);
		return $this->validate( $name, $return );
	}
	
	public function password($name){
		$return = mysql_escape(md5($this->REQUEST[$name]));
		return $this->validate( $name, $return );
	}
}

?>