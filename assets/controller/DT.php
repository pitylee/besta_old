<?php
class BST_DT{
	function __construct($build, $DB, $URL){
		$this->build = $build;
		$this->DB = $DB;
		$this->URL = $URL;
	}
	
	public function database($alias, $section=null, $instances=null, $lang=null){
		$alias = strip_tags($alias);
		$where = "`Alias`='".$alias."'";
		$lang = (empty($lang)) ? $this->URL->params("lang") : $lang;
		$lang = ucfirst($lang);
		$this->DB->select("dictionary", "*", $where);
		$translated = $this->DB->getResult();
		
		$dtword = ( isset($translated[$lang]) && !empty($translated[$lang]) ) ? $translated[$lang] : "#".$alias."#";
		
		echo $dtword;
	}
	
	public function req($alias, $section=null, $instances=null, $lang=null){
		// First we set the where for the db
		$alias = strip_tags($alias);
		$where = "`Alias`='".$alias."'";
		$lang = (empty($lang)) ? $this->URL->params("lang") : $lang;
		$lang = ucfirst($lang);
		$this->DB->select("dictionary_req", "*", $where);
		$translated = $this->DB->getResult();
		
		$dtword = ( isset($translated[$lang]) && !empty($translated[$lang]) ) ? $translated[$lang] : "#".$alias."#";
		
		echo $dtword;
	}
	
	public function ini($alias, $section=null, $instances=null, $lang=null){
		// Filter the spaces and create an array from the parts by the underline
		$alias=str_replace(" ", "", $alias);
		$section=str_replace(" ", "", $section);
		
		// Define the type and the alias part 
		$section = strip_tags($section);
		$alias = strip_tags($alias);
		$lang = (empty($lang)) ? $this->URL->params("lang") : $lang;
		
		// Define the alias and section and the upper case variant from the ini
		$aliasuc = strtoupper($alias);
		$sectionuc = strtoupper($section);
		$typeuc = strtoupper($section);
		
		// Define the file and parse if exists
		$file = $_SERVER["DOCUMENT_ROOT"]."languages/lang_".$lang.".ini";
		$ini = ( is_file($file) ) ? parse_ini_file($file, true) : null;
		
		if( !empty($sectionuc)){
			$dtword = ( isset($ini[$sectionuc][$aliasuc]) && !empty($ini[$sectionuc][$aliasuc]) ) ? $ini[$sectionuc][$aliasuc] : "#".$section."_".$alias."#";
		}
		else{
			$dtword = ( isset($ini[$aliasuc]) && !empty($ini[$aliasuc]) ) ? $ini[$aliasuc] : "#".$alias."#";
		}
		
		if( $instances !== NULL ){
			if( has($dtword, "%s") ){
				$dtword = printf_array($dtword, $instances);
			}
			else{
				return "The word <b>".$dtword."</b> has not %s parameter!";
			}
		}
		
		return $dtword;
	}
	
	public function translate($alias, $section=null, $instances=null, $type, $lang=null){
		// Define the valid dictionary types
		$valid_types = $this->build->config("types", "DT");
		$valid_types = str_replace(" ", "", $valid_types);
		$valid_types = explode(",",$valid_types);
		
		$alias = strip_tags($alias);
		$lang = (empty($lang)) ? $this->URL->params("lang") : $lang;
		$type = ( $type=="normal" || empty($type) || !in_array($type, $valid_types) ) ? "database" : $type;
	
		$dtword = $this->$type($alias, $section, $instances, $lang);
		return $dtword;
	}
}
?>