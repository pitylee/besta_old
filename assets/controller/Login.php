<?php
class BST_Login{
	
	function __construct($DB, $session, $Message, $UNID, $URL, $DT){
		$this->DB = $DB;
		$this->session = $session;
		$this->UNID = $UNID;
		$this->Message = $Message;
		$this->URL = $URL;
		$this->DT = $DT;
	}
	
	private function getUser($username,$password){
		$this->DB->select("users", "*", "`Username`='".$username."' AND `Password`='".$password."'");
		return $this->DB->getResult();
	}
	private function getUsername($username){
		$this->DB->select("users", "`Username`", "`Username`='".$username."'");
		return $this->DB->getResult();
	}
	public function getUserStatus(){
		$this->DB->select("users", "`Status`", "`Username`='".$username."' AND `Password`='".$password."'");
		return $this->DB->getResult();	
	}
	public function resetUserStatus($ID, $status){
		$DB->update("users", "`Status`='".$status."'", array("ID", $UID));
	}
	
	public function isLoggedIn(){
		$loggeduser = $this->session->read($this->UNID);
		if( isset($loggeduser["User"]) ){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function blockUser(){ }
	
	
	public function createUser(){ }
	public function deleteUser(){ }
	public function updateUser(){ }
	
	public function getUserPrivileges($privilege){
		return true;
		//return false;
	}
	
	
	public function Login($username, $password){
		$this->SE = $this->session->read($this->UNID);
		$validuser = $this->getUser($username,$password);
		if($validuser){ 
			$this->SE["User"]["ID"] = $validuser["ID"];
			$this->SE["User"]["Username"] = $validuser["Username"];
			$this->SE["User"]["Email"] = $validuser["Email"];
			$this->SE["User"]["Group"] = $validuser["Group"];
			$this->session->write($this->UNID,$this->SE);
			return true;
		}
		else{
			if($this->getUsername($username) == NULL){ 
				$username = ( !empty($username) ) ? $username : "(blank)";
				$instances = array($username);
				$message = $this->DT->ini("USER_NOT_EXIST", "LOGIN", $instances);
				$this->Message->set("info", "ajax", $message);
				$this->URL->redirect(".");
			}
			else{
				$this->URL->redirect(".");
				$message = $this->DT->ini("PASSWORD_MISMATCH", "LOGIN");
				$this->Message->set("info", "ajax", $message);
				//echo "password mismatch";
			}
			return false;
		}
	}
	
	public function Logout(){
		$this->SE = $this->session->read($this->UNID);
		
		if(isset($this->SE["User"])){
			unset($this->SE["User"]);
			$this->session->write($this->UNID,$this->SE);
			return true;
		}
		
	}
	
}

?>