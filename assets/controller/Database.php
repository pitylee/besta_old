<?php
class BST_Database{
    private $con = false;               // Checks to see if the connection is active
    private $result = array();          // Results that are returned from the query

	private $db_host = "localhost";     // Database Host
	private $db_user = "root";          // Username
	private $db_pass = "";          // Password
	private $db_name = "";          // Database
	
	function __construct($config) {
		$this->db_host = $config["DB_host"];
		$this->db_user = $config["DB_user"];
		$this->db_pass = $config["DB_pass"];
		$this->db_name = $config["DB_database"];
	}
	
    /*
     * Connects to the database, only one connection
     * allowed
     */
	 
    public function connect() {
        if(!$this->con) {
            $this->myconn = @mysql_connect($this->db_host,$this->db_user,$this->db_pass);
            if($this->myconn) {
                $seldb = @mysql_select_db($this->db_name,$this->myconn);
                if($seldb) {
                    $this->con = true;
					mysql_query('SET NAMES utf8') or exit("There was an error in the Database query: " . mysql_error());
					mysql_query('SET CHARACTER SET utf8') or exit("There was an error in the Database query: " . mysql_error());
					mysql_query('SET COLLATION_CONNECTION="utf8_general_ci" ') or exit("There was an error in the Database query: " . mysql_error());
					
                    return true;
                }
                else {
                    exit("There was an error in the Database query: " . mysql_error() );
                }
            }
            else {
                exit("There was an error in the Database query: " . mysql_error() );
            }
        }
        else {
            return true;
        }
    }
	
    public function disconnect(){
		if($this->con){
			$this->myconn = @mysql_close($this->myconn);
			if($this->myconn){
				$this->con = false;
			}
		}
		else {
			exit("Connection not found: Mysql returned: " . mysql_error() );
		}
	
		return true;
	}

    /*
    * Changes the new database, sets all current results
    * to null
    */
    public function setDatabase($name)
    {
        if($this->con)
        {
            if(@mysql_close()) {
                $this->con = false;
                $this->results = null;
                $this->db_name = $name;
                $this->connect();
            }
        }

    }

    /*
    * Checks to see if the table exists when performing
    * queries
    */
    private function tableExists($table)
    {
        $tablesInDb = @mysql_query('SHOW TABLES FROM '.$this->db_name.' LIKE "'.$table.'"');
        if($tablesInDb)
        {
            if(mysql_num_rows($tablesInDb)==1)
            {
                return true;
            }
            else
            {
                exit("There was an error in the Database query: " . mysql_error() );
            }
        }
    }

    /*
    * Selects information from the database.
    * Required: table (the name of the table)
    * Optional: rows (the columns requested, separated by commas)
    *           where (column = value as a string)
    *           order (column DIRECTION as a string)
    */
    public function select($table, $rows = '*', $where = null, $order = null)
    {
        $q = 'SELECT '.$rows.' FROM `'.$table.'`';
        if($where != null)
            $q .= ' WHERE '.$where;
        if($order != null)
            $q .= ' ORDER BY '.$order;
			
        $query = @mysql_query($q);
        if($query)
        {
			$this->result = null;
            $this->numResults = mysql_num_rows($query);
            for($i = 0; $i < $this->numResults; $i++)
            {
                $r = mysql_fetch_array($query);
                $key = array_keys($r);
                for($x = 0; $x < count($key); $x++)
                {
					//echo $key[$x]."=>".$r[$key[$x]]."<br>";
                    // Sanitizes keys so only alphavalues are allowed
                    if(!is_int($key[$x]))
                    {
                        if(mysql_num_rows($query) > 1)
                            $this->result[$i][$key[$x]] = $r[$key[$x]];
                        else if(mysql_num_rows($query) < 1)
                            $this->result = null;
                        else
                            $this->result[$key[$x]] = $r[$key[$x]];
                    }
                }
            }
            //echo $q."<br/><br/>";
			return true;
        }
        else
        {
            exit("There was an error in the Database query: " . mysql_error() );
        }
    }

    /*
    * Insert values into the table
    * Required: table (the name of the table)
    *           values (the values to be inserted)
    * Optional: rows (if values don't match the number of rows)
    */
    public function insert($table,$values,$rows = null)
    {
        if($this->tableExists($table))
        {
            $insert = 'INSERT INTO `'.$table.'`';
            if($rows != null){
				$rows = str_replace(" ", "", $rows);
				$rows = explode(",",$rows);
				foreach( $rows as $key=>$value ){
					$rows[$key] = "`".$value."`";
				}
				
				$rows = implode(",",$rows);
                $insert .= ' ('.$rows.')';
            }
			
            $values = explode(",",$values);
			
			foreach( $values as $key=>$value ){
				$values[$key] = "'".$value."'";
			}
			
			$values = implode(",",$values);
            $insert .= ' VALUES ('.$values.')';
			
            $ins = @mysql_query($insert);

            if($ins)
            {
                return true;
            }
            else
            {
                exit("There was an error in the Database query: " . mysql_error() . " Q: " . $insert );
            }
        }
    }
	
	/*replace - the same as insert, as structure, does update if found*/
    public function replace($table,$values,$rows = null)
    {
        if($this->tableExists($table))
        {
            $replace = 'REPLACE INTO `'.$table.'`';
            if($rows != null){
				$rows = str_replace(" ", "", $rows);
				$rows = explode(",",$rows);
				foreach( $rows as $key=>$value ){
					$rows[$key] = "`".$value."`";
				}
				
				$rows = implode(",",$rows);
                $replace .= ' ('.$rows.')';
            }
			
            $values = explode(",",$values);
			
			foreach( $values as $key=>$value ){
				$values[$key] = "'".$value."'";
			}
			
			$values = implode(",",$values);
            $replace .= ' VALUES ('.$values.')';

            $rpl = @mysql_query($replace);
			//echo $replace. "<br/><br/>";
            if($rpl)
            {
                return true;
            }
            else
            {
                exit("There was an error in the Database query: " . mysql_error() . " Q: " . $replace );
            }
        }
    }

    /*
    * Deletes table or records where condition is true
    * Required: table (the name of the table)
    * Optional: where (condition [column =  value])
    */
    public function delete($table,$where = null)
    {
        if($this->tableExists($table))
        {
            if($where == null) {
                $delete = 'DELETE '.$table;
            }
            else{
                $delete = 'DELETE FROM '.$table.' WHERE '.$where;
            }
			
            $del = @mysql_query($delete);

            if($del)
            {
                return true;
            }
            else
            {
                exit("There was an error in the Database query: " . mysql_error() );
            }
        }
        else
        {
            exit("There was an error in the Database query: " . mysql_error() );
        }
    }

    /*
     * Updates the database with the values sent
     * Required: table (the name of the table to be updated
     *           rows (the rows/values in a key/value array
     *           where (the row/condition in an array (row,condition) )
     */
    public function update($table,$rows,$where)
    {
        if($this->tableExists($table))
        {
            // Parse the where values
            // even values (including 0) contain the where rows
            // odd values contain the clauses for the row
			if(!empty($where)){
				$where = explode(',',$where);
				//for($i = 0; $i < count($where); $i++)
				foreach($where as $key=>$value){
						if(is_string($value)){
							$wheresplit = explode("=", $value);
							if(($where[$key+1]) !== null){
								$where[$key] = "`" .$wheresplit[0]. "`='". $wheresplit[1]. "' AND ";
							}
							else{
								 $where[$key] = "`" .$wheresplit[0]. "`='". $wheresplit[1]. "'";
							}
						}
				}
				$where = implode("",$where);
			}
			else{
				$where="1";
			}

            $update = 'UPDATE `'.$table.'` SET ';
			
            $keys = array_keys($rows);
            for($i = 0; $i < count($rows); $i++)
            {
                if(is_string($rows[$keys[$i]]))
                {
                    $update .= "`".$keys[$i].'`="'.$rows[$keys[$i]].'"';
                }
                else
                {
                    $update .=  "`".$keys[$i].'`='.$rows[$keys[$i]];
                }

                // Parse to add commas
                if($i != count($rows)-1)
                {
                    $update .= ',';
                }
            }
            $update .= ' WHERE '.$where;
			
			
            $query = @mysql_query($update);
            if($query)
            {
                return true;
            }
            else
            {
				exit("There was an error in the Database query: " . mysql_error() . "Query: " . $update);
            }
        }
        else
        {
            exit("There was an error in the Database query: " . mysql_error() );
        }
    }

    /*
    * Returns the result set
    */
    public function getResult() {
        return $this->result;
    }
}
?>