<?php
class BST_Template{
	public function __construct($build, $config, $smarty, $DB, $session, $UNID, $URL, $Message, $LI, $Forms, $error, $DT){
		$this->build = $build;
		$this->config = $config;
		$this->smarty = $smarty;
		$this->DB = $DB;
		$this->session = $session;
		$this->UNID = $UNID;
		$this->URL = $URL;
		$this->Message = $Message;
		$this->login = $LI;
		$this->Forms = $Forms;
		$this->error = $error;
		$this->DT = $DT;
	}
	
	
	public function meta($type, $files){
		$typepath = $this->build->config($type, "paths");
		$adminpath = $this->build->config("URL", "ADMIN");
		
		if( isset($_GET["admin"]) ){ $path = $this->build->path("NOROOT", $adminpath, $typepath); }
		else{ $path = $this->build->path("NOROOT", $typepath); }
		
		
		if($type == "js"){
			if( $files == "" ){ $files = "generator"; }
			echo "<script type=\"text/javascript\" src=\"".$path.$files.".js\"></script>";
					
		}
		if($type == "less"){
			if( $files == "" ){ $files = "generator"; }
			echo "<link rel=\"stylesheet\" href=\"".$path.$files.".css\"/>";
					
		}
		if($type == "css"){
			if( $files == "" ){ $files = "generator"; }
			echo "<link rel=\"stylesheet\" href=\"".$path.$files.".css\"/>";	
		}
	}
	
	public function title($arg){
		$delimiter = "%s";
		$arguments = array();
		$arguments[] = $this->build->config("TITLE", "current");
		
		if( !empty($arg) ){
			foreach( explode(",", $arg) as $key=>$value ){ $arguments[] = $value; }
			$pattern = $this->build->config("TITLE_PATTERN", "current");
			$argcount = count($arguments);
			$splitcount = substr_count($pattern, $delimiter);
			$argdifference = $splitcount - $argcount;


			if( $argdifference > 0 ){
				$patternsplit = stringSplit($pattern,$delimiter,2);
				$pattern = $patternsplit[0];
				$pattern .= $delimiter;
			}
		}
		else{
			$pattern = $delimiter;
		}
		
		
		echo printf_array( $pattern, $arguments );
	}
	
	public function Smarty($debug){
		// Define smarty paths
		$this->smarty->setTemplateDir($_SERVER["DOCUMENT_ROOT"]."components");
		$this->smarty->setCompileDir($_SERVER["DOCUMENT_ROOT"]."temp/smarty");
		//$smarty->setConfigDir($_SERVER["DOCUMENT_ROOT"]."/components");
		$this->smarty->setCacheDir($_SERVER["DOCUMENT_ROOT"]."/cache");
		
		// Other smarty things
		$this->smarty->debugging = $debug;

        $this->caching = Smarty::CACHING_LIFETIME_SAVED;
		//$this->smarty->setCacheLifetime(30);
		
		$this->smarty->assign("config",$this->config);
		$this->smarty->assign("session",$this->session);
		$this->smarty->assign("UNID",$this->UNID);
		$this->smarty->assign("URL",$this->URL);
		$this->smarty->assign("Message",$this->Message);
		$this->smarty->assign("login",$this->login);
		$this->smarty->assign("Forms",$this->Forms);
		$this->smarty->assign("error",$this->error);
		$this->smarty->assign("DB",$this->DB);
		$this->smarty->assign("Template",$this);
		$this->smarty->assign("DT",$this->DT);
		
		

		// Include plugins
		$pluginfolder = $this->build->path($this->build->config("ASSETS", "PATHS"),"smarty/bstplugins/");
		$pluginfiles = glob($pluginfolder."*.php", GLOB_BRACE);
		foreach($pluginfiles as $plugin){ include($plugin); }
		
		
		// Register plugins
		$this->smarty->registerPlugin("function", "message", "smarty_bst_function_message");
		$this->smarty->registerPlugin("function", "session", "smarty_bst_function_session");
		$this->smarty->registerPlugin("function", "error", "smarty_bst_function_error");
		$this->smarty->registerPlugin("function", "meta", "smarty_bst_function_meta");
		$this->smarty->registerPlugin("function", "url", "smarty_bst_function_url");
		$this->smarty->registerPlugin("function", "content", "smarty_bst_function_content");
		$this->smarty->registerPlugin("function", "title", "smarty_bst_function_title");
		$this->smarty->registerPlugin("function", "dt", "smarty_bst_function_dt");
		
		// Register blocks
		$this->smarty->registerPlugin("block", "user", "smarty_bst_block_user");
			$this->smarty->registerPlugin("function", "userelse", "smarty_bst_function_userelse");
		$this->smarty->registerPlugin("block", "page", "smarty_bst_block_page");
			$this->smarty->registerPlugin("function", "pageelseif", "smarty_bst_function_pageelseif");
			$this->smarty->registerPlugin("function", "pageelse", "smarty_bst_function_pageelse");
		$this->smarty->registerPlugin("block", "javascript", "smarty_bst_block_javascript");
	}
}
?>