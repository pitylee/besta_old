<?php


function array_add($itemtoadd, $array) {
    foreach ($array as &$element)
        $element = $itemtoadd . $element;
    return $array;
}

function printf_array($format, $arr){ 
	ob_start();
	call_user_func_array('printf', array_merge((array)$format, $arr));
	$return = ob_get_contents();
	ob_end_clean();
	return $return;
}

function split_nth($str, $delim, $n){
	return array_map(function($p) use ($delim) {
		return implode($delim, $p);
	}, array_chunk(explode($delim, $str), $n));
}

function stringSplit($string, $search, $chunck) {
    return array_map(function($var)use($search){return implode($search, $var); },array_chunk(explode($search, $string),$chunck));
}

function scriptname(){
	$req = explode("/", $_SERVER["SCRIPT_NAME"]);
	$req = end($req);
	$req = explode(".",$req)[0];
	
	//return $req;
	return $_SERVER["SCRIPT_NAME"];
}

function NOW(){
	return date("Y-m-d H:i:s", time());
}

function writefile($file, $content, $perm="a+"){
	if (!$fp = fopen($file, $perm))
		return "couldnotopen";

	if (!fwrite($fp, $content))
		return "couldnotread";

	fclose($fp);
}
function remote_file_exists($url){
   return(bool)preg_match('~HTTP/1\.\d\s+200\s+OK~', @current(get_headers($url)));
} 			
function downloadFile($url, $newfname){
	
	$remotefile = file_get_contents($url);
	
	if ( $newf = fopen($newfname, "a+") ){

		fwrite($newf, $remotefile);
		
	}
	fclose($newf);
	fclose($rfile);
}
 
function pArray($array){
	print "<pre>";
	print_r($array);
	print "</pre>";
}

function hasTags( $str ){
    return !(strcmp( $str, strip_tags($str ) ) == 0);
}

function is_hex($hex_color){
	if(preg_match('/^[a-f0-9]{6}$/i', $hex_color)){
		return $hex_color;
	}
	else{
		return false;
	}
}

function has($which,$what){
	$which = strtolower($which);
	$what = strtolower($what);
	if (strpos($which,$what) !== false) {
		return true;
	}
	else{
		return false;
	}
}

function escape($str){ 
	$searchcode=array("<script>","<script type=\"text/javascript\">","<style type=\"text/css\">","<?php","<meta","redirect","refresh","?>","</script>","</style>","</html>");

	$replacecode=array("","","","","","","","","","","","");

	return str_replace($searchcode,$replacecode,$str);
}

function mysql_escape($str){
	return escape(mysql_real_escape_string($str));
}

function password($str){
	return escape(mysql_real_escape_string($str));
}

function url(){ 
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
	$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
	return $protocol."://".$_SERVER['SERVER_NAME'].$port.$_SERVER['REQUEST_URI']; 
} 

function domain(){ 
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : ""; 
	$protocol = strleft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s; 
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]); 
	return $protocol."://".$_SERVER['SERVER_NAME'].$port; 
} 
function strleft($s1, $s2) { 
	return substr($s1, 0, strpos($s1, $s2)); 
}

function imageFromText($text, $imagewidth, $imageheight, $fontangle, $textcolor, $backgroundcolor, $font, $fontsize, $config){
	ob_start();
	$font = $_SERVER["DOCUMENT_ROOT"].$config["Template"]["Paths"]["assets"].$config["Template"]["Paths"]["font"].$font.".ttf";
	### Convert HTML backgound color to RGB
	if( eregi( "([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})", $backgroundcolor, $bgrgb ) )
	{$bgred = hexdec( $bgrgb[1] );   $bggreen = hexdec( $bgrgb[2] );   $bgblue = hexdec( $bgrgb[3] );}

	### Convert HTML text color to RGB
	if( eregi( "([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})", $textcolor, $textrgb ) ){
		$textred = hexdec( $textrgb[1] );   $textgreen = hexdec( $textrgb[2] );   $textblue = hexdec( $textrgb[3] );
	}

	### Create image
	$im = imagecreate( $imagewidth, $imageheight );

	### Declare image's background color
	$bgcolor = imagecolorallocate($im, $bgred,$bggreen,$bgblue);

	### Declare image's text color
	$fontcolor = imagecolorallocate($im, $textred,$textgreen,$textblue);

	### Get exact dimensions of text string
	$box = @imageTTFBbox($fontsize,$fontangle,$font,$text);

	### Get width of text from dimensions
	$textwidth = abs($box[4] - $box[0]);

	### Get height of text from dimensions
	$textheight = abs($box[5] - $box[1]);

	### Get x-coordinate of centered text horizontally using length of the image and length of the text
	$xcord = ($imagewidth/2)-($textwidth/2)-2;

	### Get y-coordinate of centered text vertically using height of the image and height of the text
	$ycord = ($imageheight/2)+($textheight/2);

	### Declare completed image with colors, font, text, and text location
	imagettftext ( $im, $fontsize, $fontangle, $xcord, $ycord, $fontcolor, $font, $text );

	### Display completed image as PNG
	imagepng($im);

	### Close the image
	imagedestroy($im);
	
	$imageintovar = ob_get_clean();
	
	return $imageintovar;
}

function timeAgo($date){
	 
	if(empty($date)) {
	 
	return "No date provided";
	 
	}
	 
	$periods = array("second", "minute", "hour", "day", "week", "month", "year", "decade");
	 
	$lengths = array("60","60","24","7","4.35","12","10");
	 
	$now = time();
	 
	$unix_date = strtotime($date);
	 
	// check validity of date
	 
	if(empty($unix_date)) {
	 
	return "Bad date";
	 
	}
	 
	// is it future date or past date
	 
	if($now > $unix_date) {
	 
	$difference = $now - $unix_date;
	 
	$tense = "ago";
	 
	} else {
	 
	$difference = $unix_date - $now;
	$tense = "from now";}
	 
	for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
	 
	$difference /= $lengths[$j];
	 
	}
	 
	$difference = round($difference);
	 
	if($difference != 1) {
	 
	$periods[$j].= "s";
	 
	}
	 
	return "$difference $periods[$j] {$tense}";
 
}
?>