<?php
class BST_URL{
	function __construct($build,$REQUEST){
		$this->REQUEST = $REQUEST;
		if(isset($this->REQUEST["param"])){
			foreach( explode("/",$this->REQUEST["param"]) as $key=>$value){
				if($value){$this->getUrl[$value]=$value;}
			}
		}
		$this->build=$build;
		$this->currsite = $GLOBALS["currsite"];
	}
	
	public function params($which, $modifier=null){
		// Predefine the keys, filters and the params
		$parameters = $this->build->config("URL_PARAMETERS", $this->currsite);
		$parameters = str_replace(" ", "", $parameters);
		$parameters = explode(",", $parameters);
		
		// Check if the get param is set, or else we negotiate one
		if( isset($_GET["param"]) && !empty($_GET["param"]) ){
			$getparam = $_GET["param"];
		}
		else{
			$getparam = $this->build->config("DEFAULT_LANGUAGE", $this->currsite);
			$getparam .= "/";
			$getparam .= $this->build->config("index", $this->currsite);
			
		}
		
		// Define the filter that bypasses php, html, tpl, do, phtml extension from end
		$extfilter = $this->build->config("EXT_FILTER", "URL");
		$extfilter = explode(",", $extfilter);
		$extfilter = str_replace(" ", "", $extfilter);
		$extfilter = array_add(".", $extfilter);
		
		// Define the filter that bypasses stuff that should not be put to the url
		$filter = $this->build->config("FILTER", "URL");
		$filter = explode(",", $filter);
		$filter = str_replace(" ", "", $filter);
		$filter = array_add("-", $filter);
		
		// Replace the .html part and if it does not end in / put one in the end
		$getparam = str_replace($extfilter, "", $getparam);
		$getparam = str_replace($extfilter, "", $getparam);
		if( strrev($getparam)[0] != "/" ){ $getparam = $getparam."/"; }

		// Replace the stuff that should not be put to the url
		$getparam = str_replace($filter, "", $getparam);
		
		// Create a temporary var out of the getparam splitting the slashes
		$paramstemp = explode("/", $getparam);
		
		// We define the valid languages from the config
		$languages = $this->build->config("LANGUAGES", $this->currsite);
		$languages = explode(",", $languages);
		
		// If we do not have /lang/module check if first elem is a valid language and rearrange the params
		if( in_array($paramstemp[0], $languages) === false ){
			// The rearrangement
			foreach( $paramstemp as $key=>$value ){
				$paramstemp[$key+1] = $value;
			}
				
			$paramstemp[0] = $this->build->config("DEFAULT_LANGUAGE", $this->currsite);
		}
		
		$counter = count($parameters)-1;
		$ptcounter = count($paramstemp)-1;
		
		// Put the lang and module
		$params[0] = $paramstemp[0];
		$params[1] = $paramstemp[1];
		
		// Check not to be doubled the lang
		if($paramstemp[$ptcounter-2] != $params[0]){
			$params[$counter-1] = $paramstemp[$ptcounter-2];
		}
		else{
			$params[$counter-1] = "";
		}
		
		// Always assign the title to the params
		$params[$counter] = $paramstemp[$ptcounter-1];
		// Force create all the params
		for($key=2; $key<=$counter-2; $key++){
			if( isset($paramstemp[$key]) && !empty($paramstemp[$key]) ){
				$thisparam = $paramstemp[$key];
				if(
				$thisparam != $params[$counter] &&
				$thisparam != $params[$counter-1]
				){
					$params[$key] = $paramstemp[$key];
				}
				else{
					$params[$key] = "";
				}
			}
			else{
				 $params[$key] = "";
			}
		}
		
		// Rearrange the sorting, to be in ascending order
		ksort($params);
		
		// Combine the arrays two key value combination from key array and value array
		$params = array_combine($parameters, $params);
		
		// Check if the param is empty, and return some nice text in place
		if( $modifier !== null && empty($params[$which]) ){
			$params[$which] = "none";
		}
		
		return $params[$which];
	}
	
	public function getUrl($url){
		if(!empty($this->getUrl) && !empty($this->getUrl[$url])){
			return true;
		}
		else{
			return false;
		}
	}
	
	public function setUrl(){}
	
	public function redirect($url){
		
		if($_GET["param"] != $url ){
			header( "Location: ../" . $url ) ;
		}
		
	}
	
	public function noDirectAccess(){
		$currentfile = basename($_SERVER["PHP_SELF"], ".php");
		$directaccess = $this->build->config("DIRECT_ACCESS", "URL");
		$directaccess = explode(",",$directaccess);
		foreach( $directaccess as $key=>$value){
	
			if($value){$this->whiteList[$value]=$value;}
		}
		if(!empty($this->whiteList) && !empty($this->whiteList[$currentfile])){
			return true;
		}
		else{
			die("No direct access is allowed");
		}
	}
	
}

?>