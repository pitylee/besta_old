<?php
class BST_Session{
	function __construct($config, $DB, $DT){
		$this->DB = $DB;
		$this->config=$config;
		//session_set_save_handler(array($this, 'open'), array($this, 'close'), array($this, 'read'), array($this, 'write'), array($this, 'destroy'), array($this, 'gc'));
		register_shutdown_function('session_write_close');
	}
	
	private function getkey($id) {
		$this->DB->select("sessions", "`Key`", "`UNID`='".$id."' LIMIT 1");
		$keydata = $this->DB->getResult();
		if( !empty( $keydata ) ) { 
			$key = $keydata["Key"];
			return $key;
		} else {
			$random_key = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
			return $random_key;
		}
	}
	private function getID($id) {
		$this->DB->select("sessions", "`ID`", "`UNID`='".$id."'");
		$IDdata = $this->DB->getResult();
		
		if(!empty($IDdata["ID"])){$IDdata = $IDdata["ID"];}
		else{$IDdata=0;}
		
		return $IDdata;
	}
	
	private function encrypt($data, $key) {
	   $salt = 'cH!swe!retReGu7W6bEDRup7usuDUh9THeD2CHeGE*ewr4n39=E@rAsp7c-Ph@pH';
	   $key = substr(hash('sha256', $salt.$key.$salt), 0, 32);
	   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	   $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, serialize($data), MCRYPT_MODE_ECB, $iv));
	   return $encrypted;
	}
	
	private function decrypt($data, $key) {
	   $salt = 'cH!swe!retReGu7W6bEDRup7usuDUh9THeD2CHeGE*ewr4n39=E@rAsp7c-Ph@pH';
	   $key = substr(hash('sha256', $salt.$key.$salt), 0, 32);
	   $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
	   $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	   $decrypted = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, (base64_decode($data)), MCRYPT_MODE_ECB, $iv);
	   return $decrypted;
	}	
	
	public function start($session_name, $secure) {
		$httponly = true;

		$session_hash = 'sha512';

		if (in_array($session_hash, hash_algos())) {
		  ini_set('session.hash_function', $session_hash);
		}
		ini_set('session.hash_bits_per_character', 5);

		ini_set('session.use_only_cookies', 1);

		$cookieParams = session_get_cookie_params(); 
		session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
		session_name($session_name);
		session_start();
		session_regenerate_id(true);    
	}
	
	public function read($id) {
		$datas = $this->DB->select("sessions", "*", "`UNID`='".$id."'");
		$datar = $this->DB->getResult();
		$data=$datar["Data"];
		
		$key = $this->getkey($id);
		$data = $this->decrypt($data, $key);
		$data = unserialize($data);
		
		return $data;
	}
	
	public function write($id, $data) {
		// Get unique key
		$key = $this->getkey($id);
		$SE = $data;
		$ID = $this->getID($id);
		// Encrypt the data
		$data = $this->encrypt($data, $key);
		
		// Save the session as is
		$sessionvalues = array($ID,$id,$key,$data,NOW());
		$sessionvalues = implode(",",$sessionvalues);
		$sessionrows = "ID,UNID,Key,Data,Datetime";
		
		
		$this->DB->replace("sessions", $sessionvalues, $sessionrows);
		
		/*
		// Also save the information about the visitor
		$sessioninfovalues = array($ID, $SE["Visitor"]["Browser"],$SE["Visitor"]["IP"],$SE["Visitor"]["Screen"]["Width"]."x".$SE["Visitor"]["Screen"]["Height"]);
		$sessioninfovalues = implode(",",$sessioninfovalues);
		$sessioninforows = "`UserAgent`,`IP`, `ScreenResolution`";
		
		$this->DB->replace("session_informations", $sessioninfovalues, $sessioninforows);
		*/
		
		return true;
	}
	
	public function gc($max) {
	   $old = NOW() - $max;
	   $this->DB->delete("sessions", "`Datetime`<'".$old."'");
	   return true;
	}
	
	public function destroy($id) {
	   $this->DB->delete("sessions", "`UNID`='".$id."'");
	   setcookie("UNID", "", time()-(60*60*24), "/");
	   return true;
	}
}
?>