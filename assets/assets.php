<?php
include("config.php");

$assets = explode("/",$_GET["param"]);
$files = $assets[1];
$type = $assets[0];

if( $type == "less" || $type == "css" || $type == "js" ){
	$filename = explode(".".$type,$files);
	$filename[1] = $type;
	$file = $filename[0];
	$ext = $type;
	$files = explode("-",$file);
	if($type == "js"){
		include("compiler/js.php");
	}
	elseif($type == "less" || $type == "css"){
		include("compiler/less.php");
	}
}
elseif( $type == "jqui" ){
	// Show jquery ui image as it would be
	
	$headers = true;
	$image = $assets[1];
	$ext = explode(".", $image);
	$ext = end($ext);
	$jqueryuifolder = path($build->config("TEMPLATE_DIR", "PATHS"),$build->config("template", "current"),"jqueryui/jquery-ui-".$build->config("JQUERY_UI_VERSION", "current"),"/css/",$build->config("JQUERY_UI_TEMPLATE", "current"),"/images/");
	$fullpath = $jqueryuifolder.$image;
	
	if( error_get_last() ){
		$imagefile = imageFromText($error->show("plane", "\n", "return"), "500", "400", "0", "FF0000", "000000", "arial", "15", $config); 
	}
	else{
		if( !is_file($fullpath) ){
			$imagefile = imageFromText("jQuery UI v" . $build->config("JQUERY_UI_VERSION", "current") . " image\ndoes not exist: " . $image . "\nfor template: " . $build->config("JQUERY_UI_TEMPLATE", "current"), "500", "400", "0", "FF0000", "000000", "arial", "13", $config);
		}
		else{
			$imagefile = file_get_contents($fullpath);
		}
	}

	if( $headers ){
		if($ext == "jpg" || $ext == "jpeg"){
			header("Content-Type: image/jpeg");
		}
		elseif($ext == "gif" || $ext == "png"){
			header("Content-Type: image/".$ext);
		}
		else{
			$iserror = true;
			header("Content-Type: image/png");
			$imagefile = imageFromText("Not a valid image file: " . $ext, $width, $height, "0", "FF0000", "000000", "arial", "15", $config);
		}
	}
	
	echo $imagefile;
}
elseif( $type == "scroll" ){
	// Show jquery ui image as it would be
	
	$headers = true;	
	$image = $assets[1];
	$ext = explode(".", $image);
	$ext = end($ext);
	$scrollfolder = path($build->config("TEMPLATE_DIR", "PATHS"),$build->config("template", "current"),"scroll/");
	$fullpath = $scrollfolder.$image;
	
	if( error_get_last() ){
		$imagefile = imageFromText($error->show("plane", "\n", "return"), "500", "400", "0", "FF0000", "000000", "arial", "15", $config); 
	}
	else{
		if( !is_file($fullpath) ){
			$imagefile = imageFromText("jQuery UI v" . $build->config("JQUERY_UI_VERSION", "current") . " image\ndoes not exist: " . $image . "\nfor template: " . $build->config("JQUERY_UI_TEMPLATE", "current"), "500", "400", "0", "FF0000", "000000", "arial", "13", $config);
		}
		else{
			$imagefile = file_get_contents($fullpath);
		}
	}

	if( $headers ){
		if($ext == "jpg" || $ext == "jpeg"){
			header("Content-Type: image/jpeg");
		}
		elseif($ext == "gif" || $ext == "png"){
			header("Content-Type: image/".$ext);
		}
		else{
			$iserror = true;
			header("Content-Type: image/png");
			$imagefile = imageFromText("Not a valid image file: " . $ext, $width, $height, "0", "FF0000", "000000", "arial", "15", $config);
		}
	}
	
	echo $imagefile;
}
else{
	$headers = true;
	$iserror = false;
	$imagick = false;
	// Make a positioner of the array keys out from the count of the arr elem. and subtract one because first elem 0
	$pos = count($assets)-1;
	// The first element is always the type
	$type=$assets[0];
	// The folder in /assets/images/<folder>/ to make with we get out from the array
	$folder = $assets[1];
	
	// Check if the element before the last has :, or is one letter this meaning the last is effect
	if(has($assets[$pos-1], ":") === true || strlen($assets[$pos-1]) == 1){
	// Put the effects in its variable
		$attributes = $assets[$pos-1];
	// Check if the two before the effect are numeric this meaning they are width and height and put them in their variables
		if(is_numeric($assets[$pos-3]) === true){ $width = $assets[$pos-3];}
		if(is_numeric($assets[$pos-2]) === true){ $height = $assets[$pos-2];}
	}
	else{
	// If the one before the last is not an effect return that we won't have any effect
		$attributes = false;
	// Check if the two before the last are numeric, if true they are width and height, if not they will be false, thus meaning we don't need resizing for instance
		if(is_numeric($assets[$pos-2]) === true){ $width = $assets[$pos-2]; } else{ $width = false; }
		if(is_numeric($assets[$pos-1]) === true){ $height = $assets[$pos-1]; } else{ $height = false; }
	}
	
	// We create the subfolder just in case later we will work on
	$subfolder = "";
	// We get the file we work with. What is it's full name
	$fullname = $assets[$pos];
	
	// We check if we have more than the ones we have supplied thus meaning we have subfolders
	foreach($assets as $key=>$value){ if($value != $type && $value != $folder && $value != $width && $value != $height && $value != $attributes && $value != $fullname){ $subfolder .= $value . "/"; } }
	
	// Split the attributes value to parts, in case we have multiple, it's ok if we have only one
	$attributes = explode("-",$attributes);
	
	// Split the fullname for parts in case we want to use'em separately later on
	$filename = explode(".",$fullname)[0];
	$ext = explode(".", $fullname);
	$ext = end($ext);

	$fullpath  = $_SERVER["DOCUMENT_ROOT"];
	
	if($type == "design") {
		$fullpath = path($build->config("TEMPLATE_DIR", "PATHS"),$build->config("template", "current"), $folder, $subfolder);
	}
	else{
		$fullpath = path($build->config("UPLOADS", "PATHS"), $folder, $subfolder);
	}
	
	$fullpath .= $fullname;
	
	/*
	// Exif data
	$Messagexif = exif_read_data($fullpath, 0, true);

	// Original width and height
	$origsize = getimagesize($fullpath);
	$original=array();
	
	$original["width"]	 	= $origsize[0];
	$original["height"] 	= $origsize[1];
	$original["type"] 		= $origsize[2];
    $original["bits"]		= $origsize["bits"];
    $original["channels"]	= $origsize["channels"];
    $original["mime"]		= $origsize["mime"];
	*/
	
	if(is_file($fullpath)){
		// If we can we use Imagick
		if( class_exists("Imagick") === true && $imagick ){
			include("resizer/Imagick.php");
		}
		else{
		// If we don't have Imagick attempt to use GD
			if ( extension_loaded('gd') && gd_info()["GD Version"] == "2.0" && function_exists('gd_info') ) {
				include("resizer/GD.php");
			}
			else{
		// If we don't have GD we tell the developer if dev mode, else return original image
				if( isset($_GET["dev"]) ){
					$iserror = true;
					if($width === false && $height === false){ $width="500"; $height="400"; }
					$imagefile = imageFromText("No image manipulation libraries (Imagick,GD) exists. \n \n           Original image included when not dev!", $width, $height, "0", "FF0000", "000000", "arial", "13", $config);
				}
				else{
		// Include original image is the simplest way
					$imagefile = file_get_contents($fullpath);
				}
			}
		}
	}
	else{
		$iserror = true;
		// File not exist
		if($width === false && $height === false){ $width="500"; $height="400"; }
		$imagefile = imageFromText("Image not found: " . $subfolder . $fullname , $width, $height, "0", "FF0000", "000000", "arial", "15", $config);
	}
	
	// Developer view
	if( isset($_GET["dev"]) ){
		include("resizer/developer.php");
	}
	else{
		if( $headers ){
			if($ext == "jpg" || $ext == "jpeg"){
				header("Content-Type: image/jpeg");
			}
			elseif($ext == "gif" || $ext == "png"){
				header("Content-Type: image/".$ext);
			}
			else{
				$iserror = true;
				header("Content-Type: image/png");
				$imagefile = imageFromText("Not a valid image file: " . $ext, $width, $height, "0", "FF0000", "000000", "arial", "15", $config);
			}
		}

	
		if(	$error->show("plane", "\n", "return") && $iserror === false){
			if( $width === false && $height === false ){ $width="500"; $height="400"; }
			$imagefile = imageFromText($error->show("plane", "\n", "return"), $width, $height, "0", "FF0000", "000000", "arial", "15", $config);
		}
		
		echo $imagefile;
	}
	
}
?>