<?php
	if( isset($_GET["admin"]) ){
		$templatedir = $build->path($build->config("URL", "ADMIN"),$build->config("TEMPLATE_DIR", "PATHS"),$build->config("template", "current"));
		$jsfiles = $build->config("JQUERY_PLUGINS", "current");
		$cachename = "admin_cached_js";
	}
	else{
		$templatedir = $build->path($build->config("TEMPLATE_DIR", "PATHS"),$build->config("template", "current"));
		$jsfiles = $build->config("JQUERY_PLUGINS", "current");
		$cachename = "cached_js";
	}
	
	$cachefolder = $build->path($build->config("CACHE", "PATHS"));
	$cachefile = $cachefolder . $cachename . ".cache";
	$cachemodifiedtime = time() - filemtime($cachefile);
	
	header ("content-type: text/css; charset: UTF-8");
	header ("cache-control: must-revalidate");
	header ( "expires: " .  date("Y-m-d H:i:s", filemtime($cachefile)) );
	
	$tempcache="";
	if( $LI->getUserPrivileges("dev") || !is_file($cachefile) || $cachemodifiedtime > $build->config("TIMEOUT", "CACHE") ){
		writefile($cachefile, "", "w");
		
		$jqueryfile = $templatedir.$build->config("JS", "PATHS")."/jquery-".$build->config("JQUERY_VERSION", "current").".js";
		$jqueryurl = "http://code.jquery.com/jquery-".$build->config("JQUERY_VERSION", "current").".js";
		$tempjquery = remote_file_exists($jqueryurl);
		$jqueryuifolder = $templatedir."jqueryui/jquery-ui-".$build->config("JQUERY_UI_VERSION", "current")."/js/";
		$jqueryuifile = $jqueryuifolder . "jquery-ui-".$build->config("JQUERY_UI_VERSION", "current").".min.js";
		
		// Include jQuery or download from the site if there is
		if( is_file($jqueryfile) ){
			// If exists on server
			$tempcache .= file_get_contents($jqueryfile);
			$tempcache .= "\n\n";
			$jquery = true;
		}
		elseif( $tempjquery === true ){
			// If does not exists and found on jQuery repository
			downloadFile($jqueryurl, $jqueryfile);
			$tempcache .= file_get_contents($jqueryfile);
			$tempcache .= "\n\n";
			$jquery = true;
		}
		else{
			
			// Not found and could not be downloaded show comment and log to console
			$tempcache .= "/* \n jQuery file not found: v".$build->config("JQUERY_VERSION", "current")." nor could be downloaded from the jQuery site: \n " .$jqueryurl . " \n*/\n\n";
			//$Message->set("jQuery version not found: ".$build->config("JQUERY_VERSION", "current"));
			
			$Message->set("<script type=\"text/javascript\">console.log(\"jQuery version ".$build->config("JQUERY_VERSION", "current")." not found nor could be downloaded from the jQuery site \");</script>");
			
			$jquery = false;
		}
		
		// Show error in comment
		if( error_get_last() ){
			echo "/*\n"; $error->show("plane", "\n", "echo"); echo "\n*/\n\n";
		}
		
		// If jquery exists go get the js files
		if($jquery)
		
		// First of all include the UI just in case
		if( is_file($jqueryuifile) ){
			$tempcache .= file_get_contents($jqueryuifile);
			$tempcache .= "\n\n";
		}
		else{
			if( !is_dir($jqueryuifolder) ){
				$jquimessage = "jQuery UI v".$build->config("JQUERY_UI_VERSION", "current") . " not found ammong the files. \n May be a mismatch, check if the folder name is jquery-ui-".$build->config("JQUERY_UI_VERSION", "current")."";
			}
			else{
				$jquimessage = "jQuery UI v".$build->config("JQUERY_UI_VERSION", "current") . " not found. The file jquery-ui-".$build->config("JQUERY_UI_VERSION", "current").".min.js may be missing";
			}
			
			$tempcache .= "/* " . $jquimessage . " */\n\n";
			//$Message->set($jquimessage);
			
			$Message->set("<script type=\"text/javascript\">console.log(\"".$jquimessage."\");</script>");
		}

		// Include all files
		$jsfiles = explode(",",$jsfiles);
		foreach($jsfiles as $file){
			$filetoinclude = $templatedir.$type;
			
			$filename = "jquery.".$file;
			$filetoinclude .= "/jquery.";
			
			$filetoinclude .= $file.".".$type;
			
			if( is_file($filetoinclude) ){
				$tempcache .= "/* " . $file.".".$type." */\n";
				$tempcache .= file_get_contents($filetoinclude);
				$tempcache .= "\n\n";
			}
			else{
				$tempcache .= "/* not found " . $file.".".$type ." */\n\n";
			}
		}
		
		// We include the settings file, or create if it does not exist
		$filetoinclude = $templatedir.$type;
		$filetoinclude .= "/jquery.settings.js";
		
		$tempcache .= "\n\n/* The real jQuery */\n";
		if( !is_file($filetoinclude) ){
			$content = "$(document).ready(function(){\n\t// Stuff\n});";
			$fp = fopen($filetoinclude,"a+");
			fwrite($fp,$content);
			fclose($fp);
			chmod(777, $filetoinclude);
		}
		$tempcache .= file_get_contents($filetoinclude);		
		/*
		if( $LI->getUserPrivileges("dev") === false ){
			// remove tabs, consecutivee spaces, newlines, etc.
			$tempcache = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '	', '	'), '', $tempcache);
			// remove single spaces
			$tempcache = str_replace(array(" {", "{ ", "; ", ": ", " :", " ,", ", "), array("{", "{", ";", ":", ":", ",", ","), $tempcache);
		}
		*/
		echo "/* Cached file from " . date("Y-m-d H:i:s", filemtime($cachefile)) . " */\n";
		echo $tempcache;
		
		writefile( $cachefile, $tempcache );
	}
	else{
		// Print'em out	
		$cachedcontent = file_get_contents($cachefile);
		
		echo "/* Cached file from " . date("Y-m-d H:i:s", filemtime($cachefile)) . " */\n";
		
		echo $cachedcontent;
		
	}
?>