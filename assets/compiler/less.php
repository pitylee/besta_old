<?php
	if( isset($_GET["admin"]) ){
		$templatedir = $build->path($build->config("URL", "ADMIN"),$build->config("TEMPLATE_DIR", "PATHS"),$build->config("template", "current"));
		$cachename = "admin_cached_css_less";
	}
	else{
		$templatedir = $build->path($build->config("TEMPLATE_DIR", "PATHS"),$build->config("template", "current"));
		$cachename = "cached_css_less";
	}
	$cachefolder = $_SERVER["DOCUMENT_ROOT"] . "cache/";
	$cachefile = $cachefolder . $cachename . ".cache";
	$cachemodifiedtime = time() - filemtime($cachefile);
	
	header ("content-type: text/css; charset: UTF-8");
	header ("cache-control: must-revalidate");
	header ( "expires: " .  date("Y-m-d H:i:s", filemtime($cachefile)) );
   
	$jqueryuifolder = $templatedir."jqueryui/jquery-ui-".$build->config("JQUERY_UI_VERSION", "current")."/css/".$build->config("JQUERY_UI_TEMPLATE", "current")."/";
	$jqueryuifile = $jqueryuifolder . "jquery-ui-".$build->config("JQUERY_UI_VERSION", "current").".min.css";
	$tempcache="";
	
	if( $LI->getUserPrivileges("dev") || !is_file($cachefile) || $cachemodifiedtime > $build->config("TIMEOUT", "CACHE") ){
		writefile($cachefile, "", "w");
		
		// First of all we include the main style or create it
		if( $type == "less" ){
			$filetoinclude = $templatedir.$type;
			$filetoinclude .= "/less_000_style.less";
			
			$tempcache .= "\n/* The real style */\n";
			if( !is_file($filetoinclude) ){
				$content = "body{ background: #d3d3d3; text-align: center; margin: 0; padding: 0; position: relative; }";
				$fp = fopen($filetoinclude,"a+");
				fwrite($fp,$content);
				fclose($fp);
				chmod(777, $filetoinclude);
			}
			$tempcache .= file_get_contents($filetoinclude);
			$tempcache .= "\n\n";
		}
		
		// Include predefined and required less and css files
		$reqfiles = glob($templatedir.$build->config("LESS", "PATHS")."/*.{css,less}", GLOB_BRACE);
		
		foreach($reqfiles as $file){
			$filetoinclude = end(explode("/",$file));
			$filename = end(explode("_",$filetoinclude));
			
			if( explode(".", $filename)[0]!= "style"  ){
				if( is_file($file) ){
					$tempcache .= "/* " . $filename." */\n";
					$tempcache .= file_get_contents($file);
					
					$tempcache .= "\n\n";
				}
				else{
					$tempcache .= "\n/* not found " . $filename ." */\n";
				}
			}
		}
		
		// We include the remaining required files like jQuery UI
		if( is_file($jqueryuifile) ){
			$uicss = file_get_contents($jqueryuifile);
			
			$uicss = str_replace("images/", "/images/jqui/", $uicss);
			$tempcache .= "\n".$uicss." \n";
		}
		else{
		
			if( !is_dir($jqueryuifolder) ){
				$jquimessage = "jQuery UI v".$build->config("JQUERY_UI_VERSION", "current") . " style not found ammong the files. \n May be a mismatch, check if the folder name is jquery-ui-".$build->config("JQUERY_UI_VERSION", "current")."";
			}
			else{
				$jquimessage = "jQuery UI v".$build->config("JQUERY_UI_VERSION", "current") . " not found. The file jquery-ui-".$build->config("JQUERY_UI_VERSION", "current").".css may be missing";
			}
			
			$tempcache .= "\n/* " . $jquimessage . " */\n";
			
			//$Message->set($jquimessage);
			
			$Message->set("<script type=\"text/javascript\">console.log(\"".$jquimessage."\");</script>");
		}
		
		$reqfiles = glob($templatedir.$build->config("CSS", "PATHS")."/*.{css,less}", GLOB_BRACE);
		
		foreach($reqfiles as $file){
			$filetoinclude = end(explode("/",$file));
			$filename = end(explode("_",$filetoinclude));
			
			if( explode(".", $filename)[0] != "style"  ){
				if( is_file($file) ){
					$tempcache .= "/* " . $filename." */\n";
					$tempcache .= str_replace("images/", "/design/images/scroll/", file_get_contents($file));
					
					$tempcache .= "\n\n";
				}
				else{
					$tempcache .= "\n/* not found " . $filename ." */\n";
				}
			}
		}
		
		// Write temp cache to the cache file
		$tempcache = $less->compile($tempcache);
		// remove tabs, consecutivee spaces, newlines, etc.
		$tempcache = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '	', '	'), '', $tempcache);
		// remove single spaces
		$tempcache = str_replace(array(" {", "{ ", "; ", ": ", " :", " ,", ", ", ";}"), array("{", "{", ";", ":", ":", ",", ",", "}"), $tempcache);
	
		
		echo "/* Cached file from " . date("Y-m-d H:i:s", filemtime($cachefile)) . " */\n";
		echo $tempcache;
		
		writefile( $cachefile, $tempcache );
	}
	else{
		// Print'em out	
		$cachedcontent = file_get_contents($cachefile);
		
		echo "/* Cached file from " . date("Y-m-d H:i:s", filemtime($cachefile)) . " */\n";
		
		if( $LI->getUserPrivileges("dev") ){
			$cachedcontent = $less->compile($cachedcontent);
			$formatter->indentChar = "\t";
			$less->setFormatter($formatter);
			
			echo $cachedcontent;
		}
		else{
			echo $cachedcontent;
		}
	}
?>