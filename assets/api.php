<?php
include("config.php");

if($URL->getUrl("Login")){
	if($Forms->submitForm($_REQUEST)){
		if( $LI->isLoggedIn() === false  ){
			if( $LI->Login( $Forms->text("username"), $Forms->password("password") ) ){
				$Message->set("success", "ajax", "Successfully logged in!");
				$URL->redirect("dashboard");
			}
		}
		else{
			$Message->set("info", "ajax", "You're already logged in!");
		}
	}

}
elseif($URL->getUrl("Logout")){
	if( $LI->isLoggedIn() === true  ){
		if( $LI->Logout() ){
			$Message->set("info", "ajax", "You've successfully logged out.");
			$URL->redirect(".");
		}
	}
	else{
		$Message->set("info", "ajax", "You're not logged in!");
	}
}
elseif($URL->getUrl("destroy")){
	$session->destroy($UNID);

// unset cookies
if (isset($_SERVER['HTTP_COOKIE'])) {
    $cookies = explode(';', $_SERVER['HTTP_COOKIE']);
    foreach($cookies as $cookie) {
        $parts = explode('=', $cookie);
        $name = trim($parts[0]);
        setcookie($name, '', time()-1000);
        setcookie($name, '', time()-1000, '/');
    }
}	
	
	$URL->redirect("../dashboard");
}
elseif($URL->getUrl("config")){
	$ini = parse_ini_file("config.sample.ini", true);
	
	echo "<table style=\"width:1000px;\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">";
		echo "<tr>";
			echo "<td width=\"500px\"> CONFIG NOW </td>";
			echo "<td width=\"500px\"> CONFIG FROM INI </td>";
		echo "</tr>";
		echo "<tr >";
			echo "<td>";
				pArray($config);
			echo "</td>";
			echo "<td>";
				pArray($ini);
			echo "</td>";
		echo "</tr>";
	echo "</table>";
} 

$DB->disconnect();
?>