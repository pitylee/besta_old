<?php
// Including all the libraries and classes
include("controller/Message.php");
include("controller/Session.php");
include("controller/Template.php");
include("controller/Login.php");
include("controller/Forms.php");


// Creating the session object
$session = new BST_session($config,$DB, $DT);

// In case we loaded for first time pushing into the session database information about the visitor
if(!isset($_COOKIE["UNID"]) ){
	$SE["Visitor"]["Screen"]["Width"] = "";
	$SE["Visitor"]["Screen"]["Height"] = "";
	$SE["Visitor"]["UNID"]=$UNID;
	$SE["Visitor"]["IP"] = $_SERVER['REMOTE_ADDR'];
	$SE["Visitor"]["Browser"] = $_SERVER['HTTP_USER_AGENT'];
	
	// Creating the message array to be able to store messages for them
	$SE["Message"] = array();

	// More about the visitor will be stored here.
	
	
	// We save the screen resolution into a cookie for future use
	if(!isset($_COOKIE['screen_resolution'])) {
		echo '<script type="text/javascript">width = screen.width; height=screen.height; document.cookie="screen_resolution="+width+"X"+height;document.location=".";</script>';
		
		//$Message->set("<script type=\"text/javascript\">width = screen.width; height=screen.height; document.cookie=\"screen_resolution=\"+width+\"X\"+height;document.locationa=\".\";</script>");
	}
	// We store the screen resolution of the visitor's screen pushed by javascript into cookie
	if(isset($_COOKIE['screen_resolution'])) {
		$screen_size=$_COOKIE['screen_resolution'];
		$screen_size=explode ('X', $screen_size);
		$SE["Visitor"]["Screen"]["Width"]=$screen_size[0];
		$SE["Visitor"]["Screen"]["Height"]=$screen_size[1];
	}
}

// If there were no sessions initialized yet we try to write our stuff into it
if($session->read($UNID) === false){ $session->write($UNID,$SE); }

// The error, alert and message manager object creation
$Message = new BST_Message($config,$session,$session->read($UNID),$UNID,$URL, $DT);


// Form validation system initialization
$Forms = new BST_Forms($config,$_REQUEST, $DT);

// Login system initialization, push into it the database, session, error, SE, 
$LI = new BST_Login($DB, $session, $Message, $UNID, $URL, $DT);

// If we didn't read the session out of the database, and it is saved already, we put it in our session variable
if($session->read($UNID) === true){ $SE = $session->read($UNID); }

// Predefine the parameters to assign
$parameters = $build->config("URL_PARAMETERS", "current");
$parameters = str_replace(" ", "", $parameters);
$parameters = explode(",", $parameters);

foreach($parameters as $key=>$value){
	$smarty->assign($value, $URL->params($value));
}

// Initialize the Template Engine and the smarty
$Tpl = new BST_Template($build, $config, $smarty, $DB, $session, $UNID, $URL, $Message, $LI, $Forms, $error, $DT);
?>