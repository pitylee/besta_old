<?php
$start = explode(' ', microtime() )[1] + explode(' ', microtime() )[0];

$_SERVER['DOCUMENT_ROOT'] = $_SERVER['DOCUMENT_ROOT'] . "/cms/";

// Include all the prependable files
include($_SERVER['DOCUMENT_ROOT']."assets/controller/Functions.php");
include($_SERVER['DOCUMENT_ROOT']."assets/controller/Database.php");
include($_SERVER['DOCUMENT_ROOT']."assets/controller/Error.php");
include($_SERVER['DOCUMENT_ROOT']."assets/controller/DT.php");
include($_SERVER['DOCUMENT_ROOT']."assets/controller/URL.php");
include($_SERVER['DOCUMENT_ROOT']."assets/controller/Builder.php");
include($_SERVER['DOCUMENT_ROOT']."assets/controller/Less.php");
include($_SERVER['DOCUMENT_ROOT']."assets/smarty/Smarty.class.php");

// Database configuration
$config["DB_host"]="localhost";
$config["DB_user"]="";
$config["DB_pass"]="";
$config["DB_database"]="cms";

// We set which site is the current
$currsite = (isset($_GET["admin"])) ? "admin" : "site";

// Creation database object and establishing connection to the database
$DB = new BST_Database($config);
$DB->connect();

// Initializing the template system objects, to push them into the template
$less = new lessc();
$formatter = new lessc_formatter_classic;

// Initialize the smarty class
$smarty = new Smarty();

// Creation of the builder
$build = new BST_Builder($DB);

// Push the configs into a variable
$config = $build->initialize("config.sample.ini");

// Push the template variables to the config
$template = $build->template($less);

// Check if this is the first time running the scripts
if(!isset($_COOKIE["firstrun"])){ $firstrun = true; setcookie("firstrun", "true"); echo "firstrun saved"; }else{ $firstrun = false; }

// Giving the client a unique ID for the session
if(!isset($_COOKIE["UNID"]) ){ $UNID = md5(uniqid( rand(), true )); setcookie("UNID", $UNID); header("Location: ."); } else{ $UNID=$_COOKIE["UNID"]; }


// Now that we have all the stuff defined, we can initiate the classes

// Creating the URL object, pushing the request and the config into it
$URL = new BST_URL($build,$_REQUEST);

// Initialize the dictionary 
$DT = new BST_DT($build, $DB, $URL);

// Error reporting
$error = new BST_Error("127.0.0.1", 0, 0, NULL, "logs/error.log", $DB, $DT);
$error->set();

// Now we can initiate
include("init.php");

// We add the lang param for the config suffix choice
$config["DB_lang_suffix"] = $config["DB_suffix"]."_".$URL->params("lang");

$Tpl->Smarty(false, $build);

// Direct acces prohibited
$URL->noDirectAccess();
?>