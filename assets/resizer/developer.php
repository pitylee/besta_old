<?php

print("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">
<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">
	<head>
		<meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\">
		<title>Developer view of ".$fullname." picture.</title>
		<style type=\"text/css\">html,body{margin:0;padding:0;text-align:center;} table.picinfo{ margin: 20px auto 20px auto; }</style>
	</head>
	<body>");
	if( $LI->isLoggedIn() && $LI->getUserPrivileges("dev") ){
		print("<table class=\"picinfo\" style=\"width: 600px; color: #242424; background: #E6E6E6; border: 1px dotted #808080;\">
			<tr>
				<td style=\"text-align:center; height:50px; background:#d4d4d4;\" colspan=\"2\">There are a total of ".$pos." parameter elements.</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Folder</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$folder."</td>
			</tr>");
			
			
			if($subfolder != ""){
				print("<tr>
					<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Subfolder</td>
					<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$subfolder."</td>
				</tr>");
			}
			print("<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Original size (width x height)</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$original["height"]." x ".$original["height"]."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Supplied size (width x height)</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$width." x ".$height."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Effects</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">");
				foreach($attributes as $key=>$value){ print($value . "<br/>"); }
				echo "</td>";
			print("</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Full name</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$fullname."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Bits</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$original["bits"]."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Channels</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".var_dump($original["channels"])."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Mime</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$original["mime"]."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Extension</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".$Messagext."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Full path</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">".($fullpath)."</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Creation date</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">"); if(is_file($fullpath)){ $date=date("F j, Y, g:i A", filectime($fullpath)); echo $date . " (".timeAgo($date).")"; } print("</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Modification date</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">"); if(is_file($fullpath)){ $date=date("F j, Y, g:i A", filemtime($fullpath)); echo $date . " (".timeAgo($date).")"; } print("</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">Direct link</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">");
					$directlink = explode($config["Template"]["assets"] . $pathtype, $fullpath)[1];
					echo "<a href=\"/".$directlink."\" target=\"_blank\">".$directlink."</a>";
				print("</td>
			</tr>
			<tr>
				<td style=\"height: 30px; text-align: left; vertical-align:middle; border-right: 1px dotted black; padding-left:10px;\">EXIF Data</td>
				<td style=\"text-align:left; vertical-align:middle; padding-left: 10px;\">");
				
				if( $Messagexif === false ){ echo "No header data found."; }
				foreach ($Messagexif as $key => $section) {
					foreach ($section as $name => $val) {
						echo "<b>" . $key . "</b> : " . $name . ":". $val . " <br/>";
					}
				}
				print("</td>
			</tr>
			<tr>
				<td colspan=\"2\" style=\"height:200px;background: #EFEFEF; text-align: center; vertical-align:middle;\" >
				<img width=\"");
				if($width<1000){ echo ($width); }
				elseif($width>1000){ echo ($width/1.2); }
				elseif( $width>2000 ){echo ($width/2);}
				elseif( $width>3000 ){echo ($width/3);}
				else{echo ($width/6);}
				print("\"src=\"data:image/png;base64,".base64_encode($imagefile)."\"/>
				</td>
			</tr>
		</table>");
		}
		else{ echo "<b>You must be logged in, and have dev user priviledge!</b> <br/> If you consider you have the permission to view this page, please login here: <a href=\"".$config["adminurl"]."\"/>Login page</a> "; }
	print("</body>
</html>");

?>