<?php
// {message method="set" type="info" appear="ajax" content="My message"}
// {message method="get" appear="ajax"}

function smarty_bst_function_message($params, &$smarty) {
	$Message = $smarty->getTemplateVars("Message");
	if( isset( $params["method"] ) ){
		if( $params["method"] == "set" ){
			if( isset( $params["type"] ) &&	!empty( $params["type"] ) && isset( $params["appear"] ) && !empty( $params["appear"] ) && isset( $params["content"] ) && !empty( $params["content"] ) ){
				$Message->set($params["type"], $params["appear"], $params["content"]);
			}
			else{
				$message = "Incorrect use of {message}, can be either missing or empty params. <br/> Correct use: <i>{message method=\"set\" type=\"info\" appear=\"ajax\" content=\"My message\"}</i>";
				$Message->set("error", "ajax", $message);
			}
		}
		elseif( $params["method"] == "get" ){
			if( isset( $params["appear"] ) &&	!empty( $params["appear"] ) ){
				$Message->get($params["appear"]);
			}
			else{
				$message = "Incorrect use of {message}, can be either missing or empty params. <br/> Correct use: <i>{message method=\"set\" type=\"info\" appear=\"ajax\" content=\"My message\"}</i>";
				$Message->set("error", "ajax", $message);
			}
		}
		else{
			$message = "{message} method: ".$params["method"]." is unknown.";
			$Message->set("error", "ajax", $message);
		}
	}
	else{
		$message = "{message} method must be set!";
		$Message->set("error", "ajax", $message);
	}
}
?>