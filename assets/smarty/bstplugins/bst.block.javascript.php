<?php
// {javascript} {/javascript}
function smarty_bst_block_javascript($params, $content, &$smarty, &$repeat) {
$capturedjavascript = $smarty->getTemplateVars("capturedjavascript");
	
    // only output on the closing tag
    if(!$repeat){
        if( isset($content) && !empty($content) ) {
			$capturedjavascript .= $content . "\n";
			$smarty->assign("capturedjavascript",$capturedjavascript);
        }
    }
}

?>