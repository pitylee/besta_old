<?php
// {user privilege="admin"} {/user}

function smarty_bst_function_userelse($params, &$smarty){
   return $smarty->left_delimiter . 'userelse' . $smarty->right_delimiter; 
}

function smarty_bst_block_user($params, $content, &$smarty, &$repeat){
	$login = $smarty->getTemplateVars("login");
	
	// Define the userelse tag, using Smarty's delimiters. 
	$else = $smarty->left_delimiter . 'userelse' . $smarty->right_delimiter; 

	// Use PHP's explode function to split the content at the point where the else tag occurs. 
	$true_false = explode($else, $content, 2); 

	// If explode has worked, the true_false array will contain the 'true' value in position zero. 
	$true = (isset($true_false[0]) ? $true_false[0] : null); 

	// If there is a 'false' value, this will be contained in position one. 
	$false = (isset($true_false[1]) ? $true_false[1] : null); 
	  
    // only output on the closing tag
    if(!$repeat){
        if( isset($content) ) {
			if( isset( $params["privilege"] ) && !empty( $params["privilege"] ) ){
				if( $login->getUserPrivileges($params["privilege"]) ){
					return $true;
				}
				else{
					return $false;
				}
			}
			else{
				if( $login->isLoggedIn() ){
					return $content;
				}
				else{
					echo "You don't have enough privilege to view this item. Please log in!";
				}
				
			}
			
        }
		else{
			echo "No content for {user} block {/user}!";
		}
    }
}

?>