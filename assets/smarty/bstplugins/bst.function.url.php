<?php
// {url get="pagetocheck"}
// {url redirect="pagetogo"}

function smarty_bst_function_url ($params, &$smarty) {
	$URL = $smarty->getTemplateVars("URL");
	
	if( isset( $params["method"] ) && !empty( $params["method"] ) ){
		if( $params["method"] == "redirect" && isset($params["url"]) ){
			//echo "redirectto " . $params["url"];
			$URL->redirect($params["url"]);
		}
		elseif( $params["method"] == "get" && isset($params["type"]) && isset($params["url"]) ){
			if( $params["url"] == $URL->params($params["type"]) ){
				return true;
			}
			else{
				return false;
			}
		}
	}
	else{
		echo "You must set the show parameter for {url}";
	}
}
?>