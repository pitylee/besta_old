<?php
// {content assign="varname" method="select" table="table" rows="*" where="asd" order="1" limit="1"}
// {content assign="varname" method="insert" table="table" values="asd,asd" rows="*"}
function smarty_bst_function_content ($params, &$smarty) {
	$DB = $smarty->getTemplateVars("DB");
	$config = $smarty->getTemplateVars("config");
	
	if( isset( $params["method"] ) && !empty( $params["method"] ) ){
		// If we have suffix=no param, thus we don't want a table to be multilingual, we can bypass the suffix
		if( isset($params["suffix"]) && $params["suffix"] == "no" ){
			$table = $config["DB_prefix"].$params["table"];
		}
		else{
			$table = $config["DB_prefix"].$params["table"].$config["DB_lang_suffix"];
		}
		
		if( $params["method"] == "select" ){
			if( isset($params["table"]) && !empty($params["table"]) ){
				if( isset($params["order"]) && !empty($params["order"]) ){
					$order = $params["order"];
				}
				else{
					$order = "''";
				}
				
				if( isset($params["limit"]) && !empty($params["limit"]) ){
					$order .= " LIMIT " . $params["limit"];
				}
				
				
				// We make the magic to select from DB
				$DB->select($table, $params["rows"], $params["where"], $order);
				
				// Put it into assign param named variable, or else into content variable
				if( isset($params["assign"]) && !empty($params["assign"]) ){
					$assign = $params["assign"];
				}
				else{
					$assign = "content";
				}
				
				$$assign = $DB->getResult();
				$smarty->assign($assign, $$assign);
			}
			else{
				return "You must set the table parameter for {content}!";
			}
		}
		elseif( $params["method"] == "insert" || $params["method"] == "replace" ){
			if( isset($params["table"]) && !empty($params["table"]) ){
				if( isset($params["values"]) && !empty($params["values"]) ){
				
					// If we have set the rows
					if( isset($params["rows"]) && !empty($params["rows"]) ){
						$rows = $params["rows"];
					}
					else{
						$rows = NULL;
					}
					
					
					// We make the magic to insert into DB
					if( $params["method"] == "insert" ){
						$DB->insert($table, $params["values"], $rows);
					}
					else{
						$DB->replace($table, $params["values"], $rows);
					}
				}
				else{
					return "You must set the rows parameter for {content}!";
				}
			}
			else{
				return "You must set the table parameter for {content}!";
			}
		}
		elseif( $params["method"] == "delete" ){
			if( isset($params["table"]) && !empty($params["table"]) ){
				if( !isset($params["where"]) ){ $params["where"] = NULL; }
				
				$DB->delete($table, $params["where"]);
			}
			else{
				return "You must set the table parameter for {content}!";
			}
		}
		elseif( $params["method"] == "update" ){
				
			if( isset($params["table"]) && !empty($params["table"]) ){
				$rows = $smarty->getTemplateVars($params["rows"]);
				
				if( !empty($rows) ){
					if( isset($params["where"]) && !empty($params["where"]) ){ $where = $params["where"]; }
					
					
					$DB->update($table, $rows, $where);
				}
				else{
					return "You must assign a valid variable to rows in {content}!";
				}
			}
			else{
				return "You must set the table parameter for {content}!";
			}
		}
	}
	else{
		return "You must set the method parameter for {content}";
	}
}
?>