<?php
// {title arguments="pagetitle"}

function smarty_bst_function_title ($params, &$smarty) {
	$config = $smarty->getTemplateVars("config");
	$Template = $smarty->getTemplateVars("Template");
	
	if( isset( $params["arguments"] ) ){
		if( empty( $params["arguments"] ) ){ $params["arguments"] = NULL; }
		$Template->title($params["arguments"]);
	}
	else{
		echo "You must set the arguments parameter for {title}";
	}
}
?>