<?php
// {user privilege="admin"} {/user}

function smarty_bst_function_error($params, &$smarty) {
	$error = $smarty->getTemplateVars("error");
	
	if( isset( $params["show"] ) && !empty( $params["show"] ) ){
		$error->show($params["show"]);
	}
	else{
		echo "You must set the show parameter for {error}";
	}
}
?>