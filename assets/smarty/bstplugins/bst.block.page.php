<?php
// {page get="url"} {/page}
function smarty_bst_function_pageelse($params, &$smarty){
   return $smarty->left_delimiter . "pageelse" . $smarty->right_delimiter; 
}
function smarty_bst_function_pageelseif($params, &$smarty){
	return $smarty->left_delimiter . "pageelseif get=\"".$params["get"]."\"" . $smarty->right_delimiter; 
}

function smarty_bst_block_page($params, $content, &$smarty, &$repeat){
	$URL = $smarty->getTemplateVars("URL");
	$elseiftag = "pageelseif";
	$elsetag = "pageelse";
	$pageelseif = $smarty->left_delimiter . $elseiftag." get=\"".$params["get"]."\"" . $smarty->right_delimiter; 
	$pageelse = $smarty->left_delimiter . $elsetag. $smarty->right_delimiter; 
	$parts = explode($pageelse, $content, 2);

	$conditionindex = count($parts);
	$if = ( isset($parts[0]) ? $parts[0] : null );
	$gets = array();
	
	if( has($if, $smarty->left_delimiter.$elseiftag) ){
		$gets[] = $params["get"];
		$regexp = "/(".$smarty->left_delimiter."\b(".$elseiftag." get)\b";
		$regexp .= "\=\"+[a-zA-Z0-9]+\")/";
		preg_match_all(@$regexp, $if, $elseifsplitter);
		
		$ifcond = $if;
		foreach( $elseifsplitter as $key=>$value ){
			$val = str_replace($smarty->left_delimiter.$elseiftag." get=\"", '', $value[$key]);
			$val = str_replace("\"", '', $val);
			$valnext = str_replace($smarty->left_delimiter.$elseiftag." get=\"", '', $value[$key+1]);
			$valnext = str_replace("\"", '', $valnext);
			
			if( !empty($val) ){ $gets[] = $val; }
			
			$elseifdelimiter = $smarty->left_delimiter.$elseiftag." get=\"".$val."\"".$smarty->right_delimiter;
			$elseifdelimiternext = $smarty->left_delimiter.$elseiftag." get=\"".$valnext."\"".$smarty->right_delimiter;
			$elseifconditioner = explode($elseifdelimiter, $ifcond);
			$elseifconditionernext = explode($elseifdelimiternext, $elseifconditioner[1]);
			
			$elseifcontainer[$val] = $elseifconditionernext[0];
		}
		
		$ifdelimiter = str_replace($smarty->left_delimiter.$elseiftag." get=\"", '', $elseifsplitter[0][0]);
		$ifdelimiter = str_replace("\"", '', $ifdelimiter);
		
		$elseifcontainer[$params["get"]] = explode($smarty->left_delimiter.$elseiftag." get=\"".$ifdelimiter."\"".$smarty->right_delimiter, $if)[0];
	}
	
	$else = (isset($parts[1]) ? $parts[1] : null);
	
    if(!$repeat){
        if( isset($content) ) {
			if( isset( $params["get"] ) && !empty( $params["get"] ) ){
			
				if( empty($gets) ){
					return $if;
				}
				elseif( !empty($gets) && $URL->params("module") == "" ){
					return $elseifcontainer[$params["get"]];
				}
				elseif( in_array($URL->params("module"), $gets) ){
		
					return $elseifcontainer[$URL->params("module")];
				}
				else{
					return $else;
				}
			}
        }
		else{
			$message = "No content for {page} block {/page}!";
			$Message->set("error", "ajax", $message);
		}
    }
}

?>