<?php
// {meta type="less" name="generator"}
function smarty_bst_function_meta($params, &$smarty) {
	$Template = $smarty->getTemplateVars("Template");
	
	if( isset( $params["type"] ) && !empty( $params["type"] ) ){
		if( !isset($params["name"]) || empty($params["name"]) ){ $params["name"]="generator"; }
		$Template->meta($params["type"], $params["name"]);
	}
	else{
		echo "You must set the show parameter for {meta}";
	}
}
?>