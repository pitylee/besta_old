<?php
// {session method="read" callback="echo"}
// {session method="write"}
// {session method="add" key="asd" content="asd"}
// {session method="edit" key="asd" content="asd"}

function smarty_bst_function_session($params, &$smarty) {
	$session = $smarty->getTemplateVars("session");
	$Message = $smarty->getTemplateVars("Message");
	$UNID = $smarty->getTemplateVars("UNID");
	
	if( isset( $params["method"] ) ){
		if( $params["method"] == "read" ){
			$SE = $session->read($UNID);
			if( $params["callback"] == "print" ){
				pArray($SE);
			}
			elseif( $params["callback"] == "return" ){
				$smarty->assign("SE",$SE);
			}
			else{
				$message = "{session} callback: ".$params["callback"]." is unknown.";
				$Message->set("error", "ajax", $message);
			}
			
		}
		elseif( $params["method"] == "write" ){
			$SE = $session->read($UNID);
			$session->write($UNID, $SE);
		}
		elseif( $params["method"] == "add" ){
			$SE = $session->read($UNID);
			if( isset($params["key"]) && !empty($params["key"]) && isset($params["content"]) && !empty($params["content"]) ){
				if( !isset($SE[$params["key"]]) ){
					$SE[$params["key"]] = $params["content"];
					$session->write($UNID, $SE);
				}
				else{
					$message = "Session variable " . $params["key"] . " already exists. You may try the edit method!";
					$Message->set("error", "ajax", $message);
				}
			}
			else{
				$message = "Incorrect use of {session}, can be either missing or empty params. <br/> Correct use: <i>{session method=\"add\" key=\"key\" content=\"value\"\"}</i>";
				$Message->set("error", "ajax", $message);
			}
		}
		elseif( $params["method"] == "edit" ){
			$SE = $session->read($UNID);
			if( isset($params["key"]) && !empty($params["key"]) && isset($params["content"]) && !empty($params["content"]) ){
				if( isset($SE[$params["key"]]) ){
					$SE[$params["key"]] = $params["content"];
					$session->write($UNID, $SE);
				}
				else{
					$message = "Session variable " . $params["key"] . " does not exists. You may try the add method!";
					$Message->set("error", "ajax", $message);
				}
			}
			else{
				$message = "Incorrect use of {session}, can be either missing or empty params. <br/> Correct use: <i>{session method=\"add\" key=\"key\" content=\"value\"\"}</i>";
				$Message->set("error", "ajax", $message);
			}			
		}
		else{
			$message = "{session} method: ".$params["method"]." is unknown.";
			$Message->set("error", "ajax", $message);
		}
	}
	else{
		$message = "{session} method must be set!";
		$Message->set("error", "ajax", $message);
	}
}
?>