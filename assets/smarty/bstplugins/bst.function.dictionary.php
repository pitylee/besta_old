<?php
// {dt alias="translateme"} - translates from db
// {dt alias="translateme" type="dev"} - translates from db dev
// {dt alias="translateme" type="ini"} - translates from ini global
// {dt alias="translateme" section="section" type="ini"} - translates from ini [section]
// {dt alias="translateme" type="normal" lang="en"} - translates from db dev

function smarty_bst_function_dt ($params, &$smarty) {
	$DT = $smarty->getTemplateVars("DT");
	
	$alias = (isset($params["alias"])) ? str_replace(" ", "", strip_tags($params["alias"])) : null;
	$section = (isset($params["section"])) ? str_replace(" ", "", strip_tags($params["section"])) : null;
	$type = (isset($params["type"])) ? str_replace(" ", "", $params["type"]) : null;
	$lang = (isset($params["lang"])) ? str_replace(" ", "", $params["lang"]) : null;
	$instances = (isset($params["instances"])) ? explode(",", str_replace(" ", "", $params["instances"])) : null;
	
	
	if( !empty( $alias ) ){
		$dtword = $DT->translate($alias, $section, $instances, $type, $lang);
		return $dtword;
	}
	else{
		return "You must set the alias parameter for {dt}";
	}
}
?>