<?php
include("assets/config.php");
$URL->noDirectAccess();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>CMS</title>
	<?php $Tpl->meta("less", "generator"); ?>
	<?php $Tpl->meta("js",  "generator"); ?>
	<?php $Message->get("script"); ?>
</head>
<body>
<a href="api/Session/destroy" style="position:absolute;left:0px;">Destroy this session</a>
<div class="debug"></div>
<?php

if( $LI->isLoggedIn() === false ){
	print("
		<div class=\"logincontainer\">
			<form action=\"api/Login\" method=\"post\">
				<input class=\"input_text\" type=\"text\" name=\"username\" value=\"\" /> <br/> <br/>
				<input class=\"input_text\" type=\"password\" name=\"password\" value=\"\" /> <br/> <br/>
				<input class=\"input_button\" type=\"submit\" name=\"submit\" value=\"Login\" />
			</form>
		</div>
	");
}
else{
	if( $URL->getUrl("dashboard") ){
		echo "Welcome to dashboard!";
		$Message->set("info", "ajax", "Message when on dashboard.");
	}
	elseif( $URL->getUrl("session") ){
		pArray($session->read($UNID));
	}
	elseif( $URL->getUrl("settings") ){
		echo "Settings page";
	}
	else{
		$Message->set("info", "ajax", "The site you have requested does not exist!");
		//$URL->redirect("404");
	}
	
	echo "<br/><br/>";
	echo "<a title=\"asd\" href=\"session\">Session</a>  ";
	echo "<a href=\"settings\">Settings</a>  ";
	echo "<a href=\"doesnt\">Exists?</a>  ";
	echo "<a href=\"api/Logout\">Logout</a>  ";
}

$Message->get("ajax");
$error->show("detailed");

$finish = explode(' ', microtime() )[1] + explode(' ', microtime() )[0];
$loadtime = round(( $finish - $start ), 6);
$SE = $session->read($UNID);
$SE["Loadtime"] = $loadtime;
$session->write($UNID, $SE);
echo 'Page generated in '.$loadtime.' seconds.';
?>
</body>
</html> <?php
$DB->disconnect();
?>