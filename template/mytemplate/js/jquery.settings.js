$(document).ready(function(){
    $( "input[type=submit], button" ).button();
	$( document ).tooltip();
	$( "#tabs" ).tabs();
	$( "#slider" ).slider();
	$( "#progressbar" ).progressbar({
      value: 37
    });
	$( "#menu" ).menu();
	$( "#dialog" ).dialog();
});