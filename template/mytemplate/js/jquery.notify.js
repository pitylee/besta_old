var myMessages = ['error','warning','info','success'];
var toppos = 0;

function hideAllMessages(){
	var messagesHeights = new Array();
	for (i=0; i<myMessages.length; i++) {
		messagesHeights[i] = $('.BST_Message-' + myMessages[i]).outerHeight(); // fill array
		$('.BST_Message-' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport	  
	}
}

function showMessage(type){
	var key = $.map(myMessages, function(element,index) { if( element == type){ return index } });

	var prevheight = $(".BST_Message-"+myMessages[key-1]).outerHeight();
	toppos = toppos + prevheight;
	
	hideAllMessages();
	$('.BST_Message-'+type).show().animate({top:toppos}, 500);
}

		
$(window).load(function(){
	// Show message
	for(var i=0;i<myMessages.length;i++){
		showMessage(myMessages[i]);
	}
	 // When message is clicked, hide it
	$('.BST_Message').on("click", function(){
		$(this).animate({top: -$(this).outerHeight()}, 500);
	});
});  