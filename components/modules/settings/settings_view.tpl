<div class="settings">
	<div class="settings_title">GLOBAL</div>
	{foreach from=$config key=key item=value}
		{if is_array($value)}
			<div class="settings_title">{$key}</div>
			{foreach from=$value key=subkey item=subvalue}
			<div class="setting_container">
				{$subkey} : {$subvalue}
			</div>
			{/foreach}
		{else}
			<div class="setting_container">
				{$key} : {$value}
			</div>
		{/if}
		
	{/foreach}
</div>