{$modulecontroller = "modules/{$URL->params("module")}/{$URL->params("module")}_controller.tpl"}
{if is_file("components/{$modulecontroller}")}
	{include file="{$modulecontroller}" scope="parent"}
{else}
	{$title="404: Not found"}
{/if}
{if empty($title)} {$title=""} {/if}

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<title>{title arguments="{$title}"}</title>
	{meta type="less" name="generator"}
	{meta type="js" name="generator"}
	{message method="get" appear="script"}
</head>
<body>
{include file="elements/header.tpl" scope="parent"}
{include file="elements/menu.tpl" scope="parent"}



{$modulecontroller = "modules/{$URL->params("module")}/{$URL->params("module")}_controller.tpl"}
{if is_file("components/{$modulecontroller}")}
	{include file="elements/content.tpl" scope="parent"}
{else}
	{include file="error/404.tpl" scope="parent"}
{/if}
{include file="elements/block_left.tpl" scope="parent"}
{include file="elements/block_right.tpl" scope="parent"}
{include file="elements/footer.tpl" scope="parent"}

<script type="text/javascript">
$(document).ready(function(){
	{$capturedjavascript}
});
</script>
</body>
</html>