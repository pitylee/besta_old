<div class="site_error">
	<h1 class="error_title">{dt section="error" alias="title_404" type="ini"} {$module}</h1>
	<p>{dt section="error" alias="DETAILED_404" type="ini"}</p>
	<table class="error_table" border="1" cellspacing="0" cellpadding="0">
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">lang</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$lang}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">module</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$module}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">category</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$category}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">subcategory</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$subcategory}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">subsubcategory</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$subsubcategory}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">subsubsubcategory</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$subsubsubcategory}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">item</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$item}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">id</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$id}</td>
		</tr>
		<tr>
			<td class="error_param" style="text-align: center; vertical-align: middle;">title</td>
			<td class="error_param_value" style="text-align: left; vertical-align: middle;">{$urltitle}</td>
		</tr>
		<tr>
			<td colspan="2" class="error_param" style="text-align: center; vertical-align: middle;">
				<b>GET: </b> {$smarty.get.param}
			</td>
		</tr>
		<tr>
			<td colspan="2" class="error_param" style="text-align: center; vertical-align: middle;">
				<a href="/{$lang}/{$config.URL.index}">{title arguments=""}</a>
			</td>
		</tr>
	</table>

</div>
