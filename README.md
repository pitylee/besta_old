# bestudion-CMS---old-version

This is an old project, a Smarty Driven MVC CMS which was never released, may contain useful snippets through.

It uses php lessphp v0.3.9 by http://leafo.net/lessphp for less compiling.
Has built in multilanguage support, ability to translate integrated.
For more information, please visit the wiki!

Released under GNU General Public License.
Feel free to use it, if you find that you may deploy on a huge project, let me know.

You may find a working demo on the following links, althrough not guaranteed:
http://oldcms.d.bestudion.net
http://oldcms.dev.bestudion.net
http://besta.bestudion.net

Questions, suggestions are always welcome. :) 

